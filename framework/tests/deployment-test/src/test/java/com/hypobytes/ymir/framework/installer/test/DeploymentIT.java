/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.framework.installer.test;

import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Tests deployment of plugins.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class DeploymentIT {
	
	/**
	 * Basic GBean deployment test.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"deployment"}, testName="GBean Deployment Test")
	@Parameters({"url", "objectName"})
	public void deploymentTest(String url, String objectName) throws Exception {
		JMXServiceURL srv = new JMXServiceURL(url);
		JMXConnector jmx = JMXConnectorFactory.connect(srv);
		MBeanServerConnection mbc = jmx.getMBeanServerConnection();
		ObjectName name = new ObjectName(objectName);
		Set<ObjectInstance> beans = mbc.queryMBeans(name, null);
		Assert.assertNotNull(beans);
		Assert.assertEquals(beans.size(), 1);
		ObjectInstance obj = beans.iterator().next();
		Assert.assertNotNull(obj);
	}
}
