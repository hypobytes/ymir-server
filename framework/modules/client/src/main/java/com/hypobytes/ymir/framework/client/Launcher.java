/**
 *	Copyright 2010 HypoBytes Ltd.
 *	Copyright 2009 Apache Software Foundation
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.framework.client;

import java.io.File;

/**
 * Based on the Geronimo GShell Launcher, but works
 * with the standalone distribution.
 * @author Trygve Sanne Hardersen <trygve@hypobytes.com>
 *
 */
public class Launcher {

    private static boolean debug = Boolean.getBoolean(Launcher.class.getName() + ".debug");

    //private static String programName;
    
    private static File homeDir;

    public static void main(final String[] args) throws Exception {
        assert args != null;

        //
        // NOTE: Branding information is not available here, so we must use the basic GShell properties to configure
        //       the bootstrap loader.
        //
        
        //programName = getProgramName();
        //setProperty("program.name", programName);
        
        homeDir = getHomeDir();
        setProperty("gshell.home", homeDir.getCanonicalPath());

        File classworldsConf = getClassworldsConf();
        setProperty("classworlds.conf", classworldsConf.getCanonicalPath());

        File log4jConf = getLog4jConf();
        setProperty("log4j.configuration", log4jConf.toURI().toURL().toString());

        // Delegate to the Classworlds launcher to finish booting
        org.codehaus.plexus.classworlds.launcher.Launcher.main(args);
    }

    private static void debug(final String message) {
        if (debug) {
            System.err.println("[DEBUG] " + message);
        }
    }

    /*
    private static void warn(final String message) {
        System.err.println("[WARNING] " + message);
    }
    */

    private static void setProperty(final String name, final String value) {
        System.setProperty(name, value);
        debug(name + "=" + value);
    }

    /*
    private static String getProgramName() {
        String name = System.getProperty("program.name");
        if (name == null) {
            name = "gsh";
        }

        return name;
    }
    */

    private static File getHomeDir() throws Exception {
        String jarPath = Launcher.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        jarPath = java.net.URLDecoder.decode(jarPath, "UTF-8");

        // The jar containing this class is expected to be in <gshell.home>/bin
        File bootJar = new File(jarPath);
        return bootJar.getParentFile().getParentFile().getCanonicalFile();
    }

    private static File getClassworldsConf() throws Exception {
        return new File(homeDir, "etc/gsh-classworlds.conf");
    }

    private static File getLog4jConf() throws Exception {
        return new File(homeDir, "etc/gsh-log4j.properties");
    }
}
