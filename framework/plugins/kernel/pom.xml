<?xml version="1.0" encoding="UTF-8"?>
<!--
	Licensed to HypoBytes Ltd. under one or more contributor
	license agreements. See the NOTICE file distributed with
	this work for additional information regarding copyright
	ownership.

	HypoBytes Ltd. licenses this file to You under the
	Apache License, Version 2.0 (the "License"); you may not
	use this file except in compliance with the License.

	You may obtain a copy of the License at:

		http://www.apache.org/licenses/LICENSE-2.0
		https://hypobytes.com/licenses/APACHE-2.0

	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an
	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
	KIND, either express or implied. See the License for the
	specific language governing permissions and limitations
	under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<groupId>com.hypobytes.ymir.framework.plugins</groupId>
		<artifactId>parent</artifactId>
		<version>0.3-SNAPSHOT</version>
	</parent>
	
	<artifactId>kernel</artifactId>
	<packaging>car</packaging>
	
	<name>Ymir Framework :: Kernel Plugin</name>
	<description>Based on the Geronimo j2ee-system config, sets up the kernel.</description>

	<dependencies>
		<!-- External deps -->
		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.geronimo.framework</groupId>
			<artifactId>geronimo-kernel</artifactId>
			<version>${gvrsn}</version>
			<exclusions>
				<!-- We have excluded ASM from the Geronimo distro -->
				<!-- Making sure we don't pick it up -->
				<exclusion>
					<groupId>asm</groupId>
					<artifactId>asm</artifactId>				
				</exclusion>
				<exclusion>
					<groupId>asm</groupId>
					<artifactId>asm-commons</artifactId>				
				</exclusion>
				<!-- We pick up cglib here -->
				<!--
				<exclusion>
					<groupId>cglib</groupId>
					<artifactId>cglib-nodep</artifactId>
				</exclusion>
				-->
				<!-- Make sure we don't pick up xstream and xpp3 here -->
				<!-- For some reason this makes it impossible to deploy apps with xstream deps -->
				<!--
				<exclusion>
					<groupId>com.thoughtworks.xstream</groupId>
					<artifactId>xstream</artifactId>				
				</exclusion>
				<exclusion>
					<groupId>xpp3</groupId>
					<artifactId>xpp3_min</artifactId>				
				</exclusion>
				-->
				<!-- We have excluded geronimo-crypto from the Geronimo distro -->
				<!-- Making sure we don't pick it up -->
				<exclusion>
					<groupId>org.apache.geronimo.framework</groupId>
					<artifactId>geronimo-crypto</artifactId>				
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.geronimo.framework</groupId>
			<artifactId>geronimo-common</artifactId>
			<version>${gvrsn}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.geronimo.framework</groupId>
			<artifactId>geronimo-config-groovy-transformer</artifactId>
			<version>${gvrsn}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.geronimo.framework</groupId>
			<artifactId>geronimo-system</artifactId>
			<version>${gvrsn}</version>
			<!-- We pick up geronimo-cli here -->
			<exclusions>
				<exclusion>
					<groupId>org.apache.geronimo.framework</groupId>
					<artifactId>geronimo-crypto</artifactId>				
				</exclusion>
				<exclusion>
					<groupId>org.apache.geronimo.specs</groupId>
					<artifactId>geronimo-jaxb_2.1_spec</artifactId>				
				</exclusion>
				<exclusion>
					<groupId>org.apache.geronimo.specs</groupId>
					<artifactId>geronimo-stax-api_1.0_spec</artifactId>				
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.geronimo.framework</groupId>
			<artifactId>geronimo-core</artifactId>
			<version>${gvrsn}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.xbean</groupId>
			<artifactId>xbean-reflect</artifactId>
		</dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all-minimal</artifactId>
        </dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jul-to-slf4j</artifactId>
		</dependency>
        <!-- Build order hint -->
        <dependency>
            <groupId>com.hypobytes.ymir.framework.plugins</groupId>
            <artifactId>bootstrap</artifactId>
            <version>${project.version}</version>
            <type>car</type>
            <scope>provided</scope>
        </dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.geronimo.buildsupport</groupId>
				<artifactId>car-maven-plugin</artifactId>
				<configuration>
					<deploymentConfigs>
						<deploymentConfig>${gbeanDeployerBootstrap}</deploymentConfig>
					</deploymentConfigs>
					<deployerName>${gbeanDeployerBootstrapName}</deployerName>
					<category>Ymir Framework</category>
                    <instance>
                        <plugin-artifact>
                            <config-xml-content>
                                <gbean name="DefaultThreadPool">
                                    <attribute name="keepAliveTime">${ThreadPoolKeepAlive}</attribute>
                                    <attribute name="minPoolSize">${ThreadPoolMinSize}</attribute>
                                    <attribute name="maxPoolSize">${ThreadPoolMaxSize}</attribute>
                                </gbean>
                            </config-xml-content>
                            <config-substitution key="ThreadPoolKeepAlive">30000</config-substitution>
                            <config-substitution key="ThreadPoolMinSize">50</config-substitution>
                            <config-substitution key="ThreadPoolMaxSize">500</config-substitution>
                            <artifact-alias key="org.apache.geronimo.framework/j2ee-system//car">
                            	com.hypobytes.ymir.framework.plugins/kernel/${project.version}/car
                           	</artifact-alias>
                            <artifact-alias key="org.apache.geronimo.framework/j2ee-system/${gvrsn}/car">
                            	com.hypobytes.ymir.framework.plugins/kernel/${project.version}/car
                            </artifact-alias>
                        </plugin-artifact>
                    </instance>
					<archive>
						<manifestEntries>
							<Endorsed-Dirs>lib/endorsed</Endorsed-Dirs>
							<Extension-Dirs>lib/ext</Extension-Dirs>
						</manifestEntries>
						<manifest>
							<mainClass>org.apache.geronimo.cli.daemon.DaemonCLI</mainClass>
						</manifest>
					</archive>
                    <!--
                    NOTE: This list of dependencies is non-transitive.
                    -->
                    <classpath>
                        <element>
                            <groupId>org.apache.geronimo.framework</groupId>
                            <artifactId>geronimo-cli</artifactId>
                            <version>${gvrsn}</version>
                            <classpathPrefix>../repository/org/apache/geronimo/framework/geronimo-cli/${gvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.apache.geronimo.framework</groupId>
                            <artifactId>geronimo-kernel</artifactId>
                            <version>${gvrsn}</version>
                            <classpathPrefix>../repository/org/apache/geronimo/framework/geronimo-kernel/${gvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.apache.geronimo.framework</groupId>
                            <artifactId>geronimo-config-groovy-transformer</artifactId>
                            <version>${gvrsn}</version>
                            <classpathPrefix>../repository/org/apache/geronimo/framework/geronimo-config-groovy-transformer/${gvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.codehaus.groovy</groupId>
                            <artifactId>groovy-all-minimal</artifactId>
                            <classpathPrefix>../repository/org/codehaus/groovy/groovy-all-minimal/1.5.8/</classpathPrefix>
                        </element>
                        <!--
                        The Java bug causing this has been fixed, see http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=5088398
                        Not sure if it's fixed in Java 6 though, should be investigated.
                        
                        NOTE: geronimo-transformer needs to be on the system classpath to avoid a potential deadlock. See GERONIMO-3141.
                        -->
                        <!--
                        <element>
                            <groupId>org.apache.geronimo.framework</groupId>
                            <artifactId>geronimo-transformer</artifactId>
                            <version>${version}</version>
                        </element>
                        -->
                        <element>
                            <groupId>commons-cli</groupId>
                            <artifactId>commons-cli</artifactId>
                            <classpathPrefix>../repository/commons-cli/commons-cli/1.2/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>cglib</groupId>
                            <artifactId>cglib-nodep</artifactId>
                            <classpathPrefix>../repository/cglib/cglib-nodep/2.2/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.slf4j</groupId>
                            <artifactId>slf4j-api</artifactId>
                            <classpathPrefix>../repository/org/slf4j/slf4j-api/${slfvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.slf4j</groupId>
                            <artifactId>slf4j-log4j12</artifactId>
                            <classpathPrefix>../repository/org/slf4j/slf4j-log4j12/${slfvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.slf4j</groupId>
                            <artifactId>jcl-over-slf4j</artifactId>
                            <classpathPrefix>../repository/org/slf4j/jcl-over-slf4j/${slfvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.slf4j</groupId>
                            <artifactId>jul-to-slf4j</artifactId>
                            <classpathPrefix>../repository/org/slf4j/jul-to-slf4j/${slfvrsn}/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>log4j</groupId>
                            <artifactId>log4j</artifactId>
                            <classpathPrefix>../repository/log4j/log4j/1.2.15/</classpathPrefix>
                        </element>
                        <element>
                            <groupId>org.apache.xbean</groupId>
                            <artifactId>xbean-reflect</artifactId>
                            <classpathPrefix>../repository/org/apache/xbean/xbean-reflect/3.6/</classpathPrefix>
                        </element>
                    </classpath>
				</configuration>
			</plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/classes</outputDirectory>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>com.hypobytes.ymir</groupId>
                                    <artifactId>branding</artifactId>
                                    <version>${hypobrand}</version>
                                </artifactItem>
                            </artifactItems>
                            <includes>**/*/*.class</includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
		</plugins>
	</build>
</project>