@REM
@REM  Licensed to HypoBytes Ltd. under one or more contributor
@REM  license agreements. See the NOTICE file distributed with
@REM  this work for additional information regarding copyright
@REM  ownership.
@REM
@REM  HypoBytes Ltd. licenses this file to You under the
@REM  Apache License, Version 2.0 (the "License"); you may not
@REM  use this file except in compliance with the License.
@REM
@REM  You may obtain a copy of the License at:
@REM
@REM  	http://www.apache.org/licenses/LICENSE-2.0
@REM  	https://hypobytes.com/licenses/APACHE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing,
@REM  software distributed under the License is distributed on an
@REM  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
@REM  KIND, either express or implied. See the License for the
@REM  specific language governing permissions and limitations
@REM  under the License.
@REM
@REM
@REM  Set environment variables relating to the execution of java commands.
@REM  It is based upon the Geronimo setjavaenv script.
@REM

@REM Begin all @REM lines with '@' in case YMIR_BATCH_ECHO is 'on'
@if "%YMIR_BATCH_ECHO%" == "on"  echo on
@if not "%YMIR_BATCH_ECHO%" == "on"  echo off

@REM Handle spaces in provided paths.  Also strips off quotes.
if defined var JAVA_HOME(
set JAVA_HOME=###%JAVA_HOME%###
set JAVA_HOME=%JAVA_HOME:"###=%
set JAVA_HOME=%JAVA_HOME:###"=%
set JAVA_HOME=%JAVA_HOME:###=%
@)
if defined var JRE_HOME(
set JRE_HOME=###%JRE_HOME%###
set JRE_HOME=%JRE_HOME:"###=%
set JRE_HOME=%JRE_HOME:###"=%
set JRE_HOME=%JRE_HOME:###=%
@)

@REM check that either JAVA_HOME or JRE_HOME are set
set jdkOrJreHomeSet=0
if not "%JAVA_HOME%" == "" set jdkOrJreHomeSet=1
if not "%JRE_HOME%" == "" set jdkOrJreHomeSet=1
if "%jdkOrJreHomeSet%" == "1" goto gotJdkOrJreHome
echo Neither the JAVA_HOME nor the JRE_HOME environment variable is defined
echo At least one of these environment variable is needed to run this program
cmd /c exit /b 1
goto end

@REM If we get this far we have either JAVA_HOME or JRE_HOME set
@REM now check whether the command requires the JDK and if so
@REM check that JAVA_HOME is really pointing to the JDK files.
:gotJdkOrJreHome
set _REQUIRE_JDK=0
if "%1" == "debug" set _REQUIRE_JDK=1
if "%_REQUIRE_JDK%" == "0" goto okJdkFileCheck

set jdkNotFound=0
if not exist "%JAVA_HOME%\bin\java.exe" set jdkNotFound=1
if not exist "%JAVA_HOME%\bin\javaw.exe" set jdkNotFound=1
if not exist "%JAVA_HOME%\bin\jdb.exe" set jdkNotFound=1
if not exist "%JAVA_HOME%\bin\javac.exe" set jdkNotFound=1
if %jdkNotFound% == 0 goto okJdkFileCheck
echo The JAVA_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
echo NB: JAVA_HOME should point to a JDK not a JRE
cmd /c exit /b 1
goto end

:okJdkFileCheck
@REM default JRE_HOME to JAVA_HOME if not set.
if "%JRE_HOME%" == "" if exist "%JAVA_HOME%\bin\javac.exe" (set JRE_HOME=%JAVA_HOME%\jre) else set JRE_HOME=%JAVA_HOME%

@REM Set standard command for invoking Java.
@REM Note that NT requires a window name argument when using start.
@REM Also note the quoting as JAVA_HOME may contain spaces.
set _RUNJAVA="%JRE_HOME%\bin\java"
set _RUNJAVAW="%JRE_HOME%\bin\javaw"
set _RUNJDB="%JAVA_HOME%\bin\jdb"

:end
@REM pause the batch file if YMIR_BATCH_PAUSE is set to 'on'
if "%YMIR_BATCH_PAUSE%" == "on" pause
