@REM
@REM  Licensed to HypoBytes Ltd. under one or more contributor
@REM  license agreements. See the NOTICE file distributed with
@REM  this work for additional information regarding copyright
@REM  ownership.
@REM
@REM  HypoBytes Ltd. licenses this file to You under the
@REM  Apache License, Version 2.0 (the "License"); you may not
@REM  use this file except in compliance with the License.
@REM
@REM  You may obtain a copy of the License at:
@REM
@REM  	http://www.apache.org/licenses/LICENSE-2.0
@REM  	https://hypobytes.com/licenses/APACHE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing,
@REM  software distributed under the License is distributed on an
@REM  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
@REM  KIND, either express or implied. See the License for the
@REM  specific language governing permissions and limitations
@REM  under the License.
@REM
@REM  This is the Ymir management script. It is based on the Apache
@REM  Geronimo script.
@REM
@REM Invocation Syntax:
@REM
@REM   ymir command [ymir_args]
@REM
@REM   For detailed usage information, just run ymir.bat without any
@REM   arguments.
@REM
@REM Environment Variable Prequisites:
@REM
@REM   YMIR_HOME   May point at your Ymir top-level directory.
@REM                   If not specified, this batch file will attempt to
@REM                   discover it relative to the location of this file.
@REM
@REM   YMIR_OPTS   (Optional) Java runtime options (in addition to
@REM                   those set in JAVA_OPTS) used when the "start",
@REM                   "stop", or "run" command is executed.
@REM
@REM   YMIR_TMPDIR (Optional) Directory path location of temporary directory
@REM                   the JVM should use (java.io.tmpdir).  Defaults to
@REM                   var\tmp (resolved to server instance directory).
@REM
@REM   YMIR_WIN_START_ARGS  (Optional) additional arguments to the Windows
@REM                            START command when the "start" command
@REM                            is executed. E.G, you could set this to /MIN
@REM                            to start Ymir in a minimized window.
@REM
@REM   JAVA_HOME       Points to your Java Development Kit installation.
@REM                   JAVA_HOME doesn't need to be set if JRE_HOME is set
@REM                   unless you use the "debug" command.
@REM                   It is mandatory either JAVA_HOME or JRE_HOME are set.
@REM
@REM   JRE_HOME        (Optional) Points to your Java Runtime Environment
@REM                   Set this if you wish to run Ymir using the JRE
@REM                   instead of the JDK (except for the "debug" command).
@REM                   Defaults to JAVA_HOME if empty.
@REM                   It is mandatory either JAVA_HOME or JRE_HOME are set.
@REM
@REM   JAVA_OPTS       (Optional) Java runtime options used when the "start",
@REM                   "stop", or "run" command is executed.
@REM                   Also see the YMIR_OPTS environment variable.
@REM
@REM   JDB_SRCPATH     (Optional) The Source Path to be used by jdb debugger.
@REM                   Defaults to %YMIR_HOME%\src
@REM
@REM   JPDA_ADDRESS    (Optional) Java runtime options used when the "jpda start"
@REM                   command is executed. The default is "8000".
@REM
@REM   JPDA_OPTS       (Optional) JPDA command line options.
@REM                   Only set this if you need to use some unusual JPDA
@REM                   command line options.  This overrides the use of the
@REM                   other JPDA_* environment variables.
@REM                   Defaults to JPDA command line options contructed from
@REM                   the JPDA_ADDRESS, JPDA_SUSPEND and JPDA_TRANSPORT
@REM                   environment variables.
@REM
@REM   JPDA_SUSPEND    (Optional) Suspend the JVM before the main class is loaded.
@REM                   Valid values are 'y' and 'n'.  The default is "n".
@REM
@REM   JPDA_TRANSPORT  (Optional) JPDA transport used when the "jpda start"
@REM                   command is executed. The default is "dt_socket".
@REM                   Note that "dt_socket" is the default instead of "dt_shmem"
@REM                   because eclipse does not support "dt_shmem".
@REM
@REM Troubleshooting execution of this batch file:
@REM
@REM   YMIR_BATCH_ECHO (Optional) Environment variable that when set to
@REM                       "on" results in batch commands being echoed.
@REM
@REM   YMIR_BATCH_PAUSE (Optional) Environment variable that when set to
@REM                        "on" results in each batch file to pause at the
@REM                        end of execution
@REM
@REM   YMIR_ENV_INFO    (Optional) Environment variable that when set to
@REM                        "on" (the default) outputs the values of
@REM                        YMIR_HOME, YMIR_TMPDIR,
@REM                        JAVA_HOME and JRE_HOME before the command is
@REM                        issued. Set to "off" if you do not want this
@REM                        information displayed.
@REM
@REM Batch files called by this batch file:
@REM
@REM   %YMIR_HOME%\bin\setenv.bat
@REM                   (Optional) This batch file is called if it is present.
@REM                   Its contents may set one or more of the above environment
@REM                   variables. It is preferable (to simplify migration to
@REM                   future Ymir releases) to set environment variables
@REM                   in this file rather than modifying Ymir's script files.
@REM
@REM   %YMIR_HOME%\bin\setjavaenv.bat
@REM                   This batch file is called to set environment variables
@REM                   relating to the java or jdb exe file to call.
@REM                   This file should not need to be modified.
@REM
@REM Exit Codes:
@REM
@REM  0        - Success
@REM  Non-zero - Error
@REM ---------------------------------------------------------------------------

@if "%YMIR_BATCH_ECHO%" == "on"  echo on
@if not "%YMIR_BATCH_ECHO%" == "on"  echo off

@setlocal enableextensions

if not "%YMIR_HOME%" == "" goto resolveHome
@REM %~dp0 is expanded pathname of the current script
set YMIR_HOME=%~dp0..

@REM resolve .. and remove any trailing slashes
:resolveHome
set CURRENT_DIR=%cd%
cd /d %YMIR_HOME%
set YMIR_HOME=%cd%
cd /d %CURRENT_DIR%

:gotHome
if exist "%YMIR_HOME%\bin\ymir.bat" goto okHome
echo The YMIR_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
cmd /c exit /b 1
goto end
:okHome

@REM Get standard environment variables
@REM Users can optionally create this file to set environment variables.
if exist "%YMIR_HOME%\bin\setenv.bat" call "%YMIR_HOME%\bin\setenv.bat"
if not %errorlevel% == 0 goto end

@REM Get standard Java environment variables (based upon Tomcat's setclasspath.bat
@REM but renamed since Ymir's classpath is set in the JAR manifest)
if exist "%YMIR_HOME%\bin\setjavaenv.bat" goto okSetJavaEnv
echo Cannot find %YMIR_HOME%\bin\setjavaenv.bat
echo This file is needed to run this program
cmd /c exit /b 1
goto end
:okSetJavaEnv
set BASEDIR=%YMIR_HOME%
call "%YMIR_HOME%\bin\setJavaEnv.bat"
if not %errorlevel% == 0 goto end

@REM Using default JAVA_OPTS if it's not set
if not "%JAVA_OPTS%" == "" goto skipDefaultJavaOpts
set JAVA_OPTS=-Xmx1024m -XX:MaxPermSize=256m
:skipDefaultJavaOpts

if not "%YMIR_TMPDIR%" == "" goto gotTmpdir
@REM A relative value will be resolved relative to each instance 
set YMIR_TMPDIR=var\tmp
:gotTmpdir

set _EXECJAVA=%_RUNJAVA%
@REM MAINCLASS required for jdb debugger as it requires the mainclass
@REM parameter. For other commands, the main class is obtained from
@REM the JAR manifest.
set MAINCLASS=org.apache.geronimo.cli.daemon.DaemonCLI
set JPDA=
set _JARFILE="%YMIR_HOME%"\bin\server.jar

if not ""%1"" == ""jpda"" goto noJpda
set JPDA=jpda
if not "%JPDA_SUSPEND%" == "" goto gotJpdaSuspend
set JPDA_SUSPEND=n
:gotJpdaSuspend
if not "%JPDA_TRANSPORT%" == "" goto gotJpdaTransport
@REM Note that "dt_socket" is the default instead of "dt_shmem"
@REM because eclipse does not support "dt_shmem".
set JPDA_TRANSPORT=dt_socket
:gotJpdaTransport
if not "%JPDA_ADDRESS%" == "" goto gotJpdaAddress
set JPDA_ADDRESS=8000
:gotJpdaAddress
if not "%JPDA_OPTS%" == "" goto gotJpdaOpts
set JPDA_OPTS=-Xdebug -Xrunjdwp:transport=%JPDA_TRANSPORT%,address=%JPDA_ADDRESS%,server=y,suspend=%JPDA_SUSPEND%
:gotJpdaOpts
set YMIR_OPTS=%YMIR_OPTS% %JPDA_OPTS%
shift
:noJpda

@REM ----- Execute The Requested Command ---------------------------------------
@if "%YMIR_ENV_INFO%" == "off" goto skipEnvInfo
echo Using YMIR_HOME:   %YMIR_HOME%
echo Using YMIR_TMPDIR: %YMIR_TMPDIR%
if "%_REQUIRE_JDK%" == "1" echo Using JAVA_HOME:       %JAVA_HOME%
if "%_REQUIRE_JDK%" == "0" echo Using JRE_HOME:        %JRE_HOME%
if not "%JPDA%" == "jpda" goto skipJpdaEnvInfo
@REM output JPDA info to assist diagnosing JPDA debugger config issues.
echo Using JPDA_OPTS:       %JPDA_OPTS%
:skipJpdaEnvInfo
:skipEnvInfo

if ""%1"" == ""debug"" goto doDebug
if ""%1"" == ""run"" goto doRun
if ""%1"" == ""start"" goto doStart
if ""%1"" == ""stop"" goto doStop

echo Usage:  ymir command [args]
echo commands:
echo   debug             Debug Ymir in jdb debugger
echo   jpda run          Start Ymir in foreground under JPDA debugger
echo   jpda start        Start Ymir in background under JPDA debugger
echo   run               Start Ymir in the current window
echo   start             Start Ymir in a separate window
echo   stop              Stop Ymir
echo.
echo args for debug, jpda run, jpda start, run and start commands:
echo        --quiet       No startup progress
echo        --long        Long startup progress
echo   -v   --verbose     INFO log level
echo   -vv  --veryverbose DEBUG log level
echo        --override    Override configurations. USE WITH CAUTION!
echo        --help        Detailed help.
echo   -s   --secure      Enable Ymir for 2 way secure JMX communication.
echo.
echo args for stop command:
echo        --user        Admin user
echo        --password    Admin password
echo        --host        Hostname of the server
echo        --port        RMI port to connect to
echo        --secure      Enable secure JMX communication
cmd /c exit /b 1
goto end

:doDebug
shift
set _EXECJAVA=%_RUNJDB%
set JDB=jdb
if not "%JDB_SRCPATH%" == "" goto gotJdbSrcPath
set JDB_SRCPATH=%YMIR_HOME%\src
:gotJdbSrcPath
echo Note: The jdb debugger will start Ymir in another process and connect to it.
echo       To terminate Ymir when running under jdb, run the "ymir stop" command
echo       in another window.  Do not use Ctrl-C as that will terminate the jdb client
echo       (the debugger itself) but will not stop the Ymir process.
goto execCmd

:doRun
shift
goto execCmd

:doStart
echo.
echo Starting Ymir in a separate window...
shift
@REM use long format of startup progress to be consistent with
@REM the unix version of the start processing
set _LONG_OPT=--long
if ""%1""==""--help"" goto setHelp
if ""%1""==""-h"" goto setHelp
if ""%1""==""-help"" goto setHelp
goto setNonHelp
:setHelp
@REM print help information in current window
set _EXECJAVA=internalLauncherNoExit.bat
goto execCmd
:setNonHelp
@REM if it is not to print help information, open another window
set _EXECJAVA=start "Ymir" /d"%YMIR_HOME%\bin" %YMIR_WIN_START_ARGS% internalLauncher.bat
goto execCmd

:doStop
shift
set _JARFILE="%YMIR_HOME%"\bin\shutdown.jar
goto execCmd

:execCmd
@REM Get remaining unshifted command line arguments and save them in the
set CMD_LINE_ARGS=
:setArgs
if ""%1""=="""" goto doneSetArgs
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto setArgs
:doneSetArgs

@REM Setup the Java programming language agent
set JAVA_AGENT_JAR=%YMIR_HOME%\bin\jpa.jar
set JAVA_AGENT_OPTS=
if exist "%JAVA_AGENT_JAR%" set JAVA_AGENT_OPTS=-javaagent:"%JAVA_AGENT_JAR%"

@REM Must reset ERRORLEVEL
cmd /c exit /b 0

@REM Execute Java with the applicable properties
if not "%JDB%" == "" goto doJDB
%_EXECJAVA% %JAVA_OPTS% %YMIR_OPTS% %JAVA_AGENT_OPTS% -Djava.endorsed.dirs="%YMIR_HOME%\lib\endorsed;%JRE_HOME%\lib\endorsed" -Djava.ext.dirs="%YMIR_HOME%\lib\ext;%JRE_HOME%\lib\ext" -Dorg.apache.geronimo.home.dir="%YMIR_HOME%" -Djava.io.tmpdir="%YMIR_TMPDIR%" -jar %_JARFILE% %_LONG_OPT% %CMD_LINE_ARGS%
goto end

:doJDB
%_EXECJAVA% %JAVA_OPTS% %YMIR_OPTS% -sourcepath "%JDB_SRCPATH%" -Djava.endorsed.dirs="%YMIR_HOME%\lib\endorsed;%JRE_HOME%\lib\endorsed" -Djava.ext.dirs="%YMIR_HOME%\lib\ext;%JRE_HOME%\lib\ext" -Dorg.apache.geronimo.home.dir="%YMIR_HOME%" -Djava.io.tmpdir="%YMIR_TMPDIR%" -classpath %_JARFILE% %MAINCLASS% %CMD_LINE_ARGS%
goto end

:end
@REM pause the batch file if YMIR_BATCH_PAUSE is set to 'on'
if "%YMIR_BATCH_PAUSE%" == "on" pause
@endlocal
cmd /c exit /b %errorlevel%
