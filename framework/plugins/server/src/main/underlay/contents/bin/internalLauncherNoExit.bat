@REM
@REM  Licensed to HypoBytes Ltd. under one or more contributor
@REM  license agreements. See the NOTICE file distributed with
@REM  this work for additional information regarding copyright
@REM  ownership.
@REM
@REM  HypoBytes Ltd. licenses this file to You under the
@REM  Apache License, Version 2.0 (the "License"); you may not
@REM  use this file except in compliance with the License.
@REM
@REM  You may obtain a copy of the License at:
@REM
@REM  	http://www.apache.org/licenses/LICENSE-2.0
@REM  	https://hypobytes.com/licenses/APACHE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing,
@REM  software distributed under the License is distributed on an
@REM  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
@REM  KIND, either express or implied. See the License for the
@REM  specific language governing permissions and limitations
@REM  under the License.
@REM
@REM  This is the Ymir lanucher script. It is based on the Apache
@REM  Geronimo script.
@REM
@if "%GERONIMO_BATCH_ECHO%" == "on"  echo on
@if not "%GERONIMO_BATCH_ECHO%" == "on"  echo off

%_RUNJAVA% %JAVA_OPTS% %GERONIMO_OPTS% %JAVA_AGENT_OPTS% -Djava.endorsed.dirs="%GERONIMO_HOME%\lib\endorsed;%JRE_HOME%\lib\endorsed" -Djava.ext.dirs="%GERONIMO_HOME%\lib\ext;%JRE_HOME%\lib\ext" -Dorg.apache.geronimo.home.dir="%GERONIMO_HOME%" -Djava.io.tmpdir="%GERONIMO_TMPDIR%" -jar %_JARFILE% %_LONG_OPT% %CMD_LINE_ARGS%
