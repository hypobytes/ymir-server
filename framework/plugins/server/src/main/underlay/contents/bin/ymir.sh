#!/bin/sh
#
#	Licensed to HypoBytes Ltd. under one or more contributor
#	license agreements. See the NOTICE file distributed with
#	this work for additional information regarding copyright
#	ownership.
#
#	HypoBytes Ltd. licenses this file to You under the
#	Apache License, Version 2.0 (the "License"); you may not
#	use this file except in compliance with the License.
#
#	You may obtain a copy of the License at:
#
#		http://www.apache.org/licenses/LICENSE-2.0
#		https://hypobytes.com/licenses/APACHE-2.0
#
#	Unless required by applicable law or agreed to in writing,
#	software distributed under the License is distributed on an
#	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#	KIND, either express or implied. See the License for the
#	specific language governing permissions and limitations
#	under the License.
#
#   This is the Ymir management script. It is based on the Apache
#   Geronimo script.
#
# Invocation Syntax:
#
#   ymir.sh command [ymir_args]
#
#   For detailed command usage information, just run ymir.sh without any
#   arguments.
#
# Environment Variable Prequisites:
#
#   YMIR_HOME   (Optional) May point at your Ymir top-level directory.
#                   If not specified, it will default to the parent directory
#                   of the location of this script.
#
#   YMIR_OPTS   (Optional) Java runtime options used when the "start",
#                   "stop", or "run" command is executed.
#
#   YMIR_OUT    (Optional) File that Ymir's stdout and stderr streams
#                   will be redirected to if Ymir is started in the
#                   background.
#                   Defaults to $YMIR_HOME/var/log/ymir.out
#
#   YMIR_PID    (Optional) Path of the file which should contains the pid
#                   of the Ymir java process, when start (fork) is used
#
#   YMIR_TMPDIR (Optional) Directory path location of temporary directory
#                   the JVM should use (java.io.tmpdir). Defaults to var/tmp 
#                   (resolved to server instance directory).
#
#   JAVA_HOME       Points to your Java Development Kit installation.
#                   JAVA_HOME doesn't need to be set if JRE_HOME is set
#                   unless you use the "debug" command.
#                   It is mandatory either JAVA_HOME or JRE_HOME are set.
#
#   JRE_HOME        Points to your Java Runtime Environment installation.
#                   Set this if you wish to run Ymir using the JRE
#                   instead of the JDK (except for the "debug" command).
#                   Defaults to JAVA_HOME if empty.
#                   It is mandatory either JAVA_HOME or JRE_HOME are set.
#
#   JAVA_OPTS       (Optional) Java runtime options used when the "start",
#                   "stop", or "run" command is executed.
#
#   JDB_SRCPATH     (Optional) The Source Path to be used by jdb debugger
#                   when the "debug" command is executed.
#                   Defaults to %YMIR_HOME%\src
#
#   JPDA_ADDRESS    (Optional) Java runtime options used when the "jpda start"
#                   command is executed. The default is 8000.
#
#   JPDA_TRANSPORT  (Optional) JPDA transport used when the "jpda start"
#                   command is executed. The default is "dt_socket".
#
#   JPDA_OPTS       (Optional) JPDA command line options.
#                   Only set this if you need to use some unusual JPDA
#                   command line options.  This overrides the use of the
#                   other JPDA_* environment variables.
#                   Defaults to JPDA command line options contructed from
#                   the JPDA_ADDRESS, JPDA_SUSPEND and JPDA_TRANSPORT
#                   environment variables.
#
#   JPDA_SUSPEND    (Optional) Suspend the JVM before the main class is loaded.
#                   Valid values are 'y' and 'n'.  The default is "n".
#
#   START_OS_CMD    (Optional) Operating system command that will be placed in
#                   front of the java command when starting Ymir in the
#                   background.  This can be useful on operating systems where
#                   the OS provides a command that allows you to start a process
#                   with in a specified CPU or priority.
#
# Troubleshooting execution of this script file:
#
#  YMIR_ENV_INFO (Optional) Environment variable that when set to "on"
#                    (the default) outputs the values of the YMIR_HOME,
#                    YMIR_TMPDIR, JAVA_HOME and
#                    JRE_HOME before the command is issued. Set to "off"
#                    if you do not want this information displayed.
#
# Scripts called by this script:
#
#   $YMIR_HOME/bin/setenv.sh
#                   (Optional) This script file is called if it is present.
#                   Its contents may set one or more of the above environment
#                   variables.  It is preferable (to simplify migration to
#                   future Ymir releases) to set environment variables
#                   in this file rather than modifying Ymir's script files.
#
#   $YMIR_HOME/bin/setjavaenv.sh
#                   This batch file is called to set environment variables
#                   relating to the java or jdb executable to invoke.
#                   This file should not need to be modified.
#
# Exit Codes:
#
#  0 - Success
#  1 - Error
# -----------------------------------------------------------------------------

# Server options
SERVER_HOST="0.0.0.0"
SERVER_PORT="1099"
SERVER_SHUTDOWN_TIMEOUT=30

_valid_pid()
{
	if [ -r "$_PID_FILE" ] ; then
		# load and check the pid
		_PID=`cat "$_PID_FILE"`
		if [[ -n `echo $_PID | grep "^[[:digit:]]*$"` ]] ; then
			assume_running=0
			# verify that the process is running
			_PID_RUN=`ps -C "$_PID" | grep -o "^.*$_PID" | grep -o "$_PID"`
			if [[ -n `echo $_PID_RUN | grep "^[[:digit:]]*$"` ]] && [ "$_PID" -eq "$_PID_RUN" ] ; then
				return 0
			else
				echo "Found process ID $_PID but no corresponding Ymir process"
				echo "Deleting stale process ID file $_PID_FILE"
				rm "-rf" "$_PID_FILE"
			fi
		else
			echo "Found invalid process ID"
			echo "Deleting corrupt process ID file $_PID_FILE"
			rm "-rf" "$_PID_FILE"
		fi
	fi
	return 1
}

_jndi_connection()
{
	_JNDI_STATE=`echo "close" | nc -v -w 10 "$SERVER_HOST" "$SERVER_PORT" 2>/dev/null | sed -e 's/^Connection to.* succeeded!$/connected/'`
	if [ "$_JNDI_STATE" = "connected" ] ; then
		return 0
	fi
	_JNDI_STATE="disconnected"
	return 1
}

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false
os400=false
case "`uname`" in
CYGWIN*) cygwin=true;;
OS400*) os400=true;;
esac

# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set YMIR_HOME if not already set
[ -z "$YMIR_HOME" ] && YMIR_HOME=`cd "$PRGDIR/.." ; pwd`

if [ -r "$YMIR_HOME"/bin/setenv.sh ]; then
  . "$YMIR_HOME"/bin/setenv.sh
fi

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin; then
  [ -n "$JAVA_HOME" ] && JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$JRE_HOME" ] && JRE_HOME=`cygpath --unix "$JRE_HOME"`
  [ -n "$JDB_SRCPATH" ] && JDB_SRCPATH=`cygpath --unix "$JDB_SRCPATH"`
  [ -n "$YMIR_HOME" ] && YMIR_HOME=`cygpath --unix "$YMIR_HOME"`
fi

# For OS400
if $os400; then
  # Set job priority to standard for interactive (interactive - 6) by using
  # the interactive priority - 6, the helper threads that respond to requests
  # will be running at the same priority as interactive jobs.
  COMMAND='chgjob job('$JOBNAME') runpty(6)'
  system $COMMAND

  # Enable multi threading
  export QIBM_MULTI_THREADED=Y
fi

# Get standard Java environment variables
# (based upon Tomcat's setclasspath.sh but renamed since Ymir's classpath
# is set in the JAR manifest)
if $os400; then
  # -r will Only work on the os400 if the files are:
  # 1. owned by the user
  # 2. owned by the PRIMARY group of the user
  # this will not work if the user belongs in secondary groups
  BASEDIR="$YMIR_HOME"
  . "$YMIR_HOME"/bin/setjavaenv.sh
else
  if [ -r "$YMIR_HOME"/bin/setjavaenv.sh ]; then
    BASEDIR="$YMIR_HOME"
    . "$YMIR_HOME"/bin/setjavaenv.sh
  else
    echo "Cannot find $YMIR_HOME/bin/setjavaenv.sh"
    echo "This file is needed to run this program"
    exit 1
  fi
fi

# Use a default JAVA_OPTS if it's not set
if [ -z "$JAVA_OPTS" ]; then
  JAVA_OPTS="-Xmx1024m -XX:MaxPermSize=256m"
fi

if [ -z "$YMIR_TMPDIR" ] ; then
  # Define the java.io.tmpdir to use for Ymir
  # A relative value will be resolved relative to each instance
  YMIR_TMPDIR=var/tmp
fi

if [ -z "$YMIR_OUT" ] ; then
  # Define the output file we are to redirect both stdout and stderr to
  # when Ymir is started in the background
  YMIR_OUT="$YMIR_HOME"/var/log/ymir.out
fi

if [ -z "$JDB_SRCPATH" ] ; then
  # Define the source path to be used by the JDB debugger
  JDB_SRCPATH="$YMIR_HOME"/src
fi

# Check that either JAVA_HOME OR JRE_HOME is set
if [ -z "$JAVA_HOME" ]; then
	if [ -d "$JRE_HOME" ]; then
		JAVA_OK="y"
	fi
else
	if [ -d "$JAVA_HOME" ]; then
		JAVA_OK="y"
	fi
fi

if [ "$JAVA_OK" != "y" ]; then
	echo "JAVA_HOME or JRE_HOME must be set to an existing JDK or JRE";
	exit 1;
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  JAVA_HOME=`cygpath --absolute --windows "$JAVA_HOME"`
  JRE_HOME=`cygpath --absolute --windows "$JRE_HOME"`
  JDB_SRCPATH=`cygpath --absolute --windows "$JDB_SRCPATH"`
  YMIR_HOME=`cygpath --absolute --windows "$YMIR_HOME"`
  YMIR_TMPDIR=`cygpath --windows "$YMIR_TMPDIR"`
  EXT_DIRS="$YMIR_HOME/lib/ext;$JRE_HOME/lib/ext"
  ENDORSED_DIRS="$YMIR_HOME/lib/endorsed;$JRE_HOME/lib/endorsed"
else
  EXT_DIRS="$YMIR_HOME/lib/ext:$JRE_HOME/lib/ext"
  ENDORSED_DIRS="$YMIR_HOME/lib/endorsed:$JRE_HOME/lib/endorsed"
fi

# ----- Execute The Requested Command -----------------------------------------
if [ "$YMIR_ENV_INFO" != "off" ] ; then
  echo "Using YMIR_HOME:   $YMIR_HOME"
  echo "Using YMIR_TMPDIR: $YMIR_TMPDIR"
  if [ "$1" = "debug" ] ; then
    echo "Using JAVA_HOME:       $JAVA_HOME"
    echo "Using JDB_SRCPATH:     $JDB_SRCPATH"
  else
    echo "Using JRE_HOME:        $JRE_HOME"
  fi
fi

LONG_OPT=
if [ "$1" = "start" ] ; then
  LONG_OPT=--long
  if [ "$YMIR_ENV_INFO" != "off" ] ; then
    echo "Using YMIR_OUT:    $YMIR_OUT"
  fi
fi

if [ "$1" = "jpda" ] ; then
  if [ -z "$JPDA_SUSPEND" ]; then
    JPDA_SUSPEND="n"
  fi
  if [ -z "$JPDA_TRANSPORT" ]; then
    JPDA_TRANSPORT="dt_socket"
  fi
  if [ -z "$JPDA_ADDRESS" ]; then
    JPDA_ADDRESS="8000"
  fi
  if [ -z "$JPDA_OPTS" ]; then
    JPDA_OPTS="-Xdebug -Xrunjdwp:transport=$JPDA_TRANSPORT,address=$JPDA_ADDRESS,server=y,suspend=$JPDA_SUSPEND"
  fi
  if [ "$YMIR_ENV_INFO" != "off" ] ; then
    echo "Using JPDA_OPTS:       $JPDA_OPTS"
  fi
  YMIR_OPTS="$YMIR_OPTS $JPDA_OPTS"
  shift
fi

# Setup the Java programming language agent
JAVA_AGENT_JAR="$YMIR_HOME/bin/jpa.jar"

# Process info
_PID=""
_PID_RUN=""
_PID_FILE="$YMIR_HOME/var/run/ymir.pid"
_JNDI_STATE=""

if [ "$1" = "debug" ] ; then
  if $os400; then
    echo "Debug command not available on OS400"
    exit 1
  else
    echo "Note: The jdb debugger will start Ymir in another process and connect to it."
    echo "      To terminate Ymir when running under jdb, run the "ymir stop" command"
    echo "      in another window.  Do not use Ctrl-C as that will terminate the jdb client"
    echo "      (the debugger itself) but will not stop the Ymir process."
    shift
    exec "$_RUNJDB" $JAVA_OPTS $YMIR_OPTS \
      -sourcepath "$JDB_SRCPATH" \
      -Djava.endorsed.dirs="$ENDORSED_DIRS" \
      -Djava.ext.dirs="$EXT_DIRS" \
      -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
      -Djava.io.tmpdir="$YMIR_TMPDIR" \
      -classpath "$YMIR_HOME"/bin/server.jar \
      org.apache.geronimo.cli.daemon.DaemonCLI $LONG_OPT "$@"
    echo "$!" > "$_PID_FILE"
  fi

elif [ "$1" = "run" ]; then
  shift
  if [ -f "$JAVA_AGENT_JAR" ]; then
      exec "$_RUNJAVA" $JAVA_OPTS $YMIR_OPTS \
        -javaagent:"$JAVA_AGENT_JAR" \
        -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
        -Djava.endorsed.dirs="$ENDORSED_DIRS" \
        -Djava.ext.dirs="$EXT_DIRS" \
        -Djava.io.tmpdir="$YMIR_TMPDIR" \
        -jar "$YMIR_HOME"/bin/server.jar $LONG_OPT "$@"
      echo "$!" > "$_PID_FILE"
  else
      exec "$_RUNJAVA" $JAVA_OPTS $YMIR_OPTS \
        -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
        -Djava.endorsed.dirs="$ENDORSED_DIRS" \
        -Djava.ext.dirs="$EXT_DIRS" \
        -Djava.io.tmpdir="$YMIR_TMPDIR" \
        -jar "$YMIR_HOME"/bin/server.jar $LONG_OPT "$@"
      echo "$!" > "$_PID_FILE"
  fi

elif [ "$1" = "start" ] ; then
  shift
  ISHelp=false
  case "$1" in
  --help) ISHelp=true;;
  -help) ISHelp=true;;
  -h) ISHelp=true;;
  esac
  if $ISHelp; then
    if [ -f "$JAVA_AGENT_JAR" ]; then
	      exec "$_RUNJAVA" $JAVA_OPTS $YMIR_OPTS \
	        -javaagent:"$JAVA_AGENT_JAR" \
	        -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
	        -Djava.endorsed.dirs="$ENDORSED_DIRS" \
	        -Djava.ext.dirs="$EXT_DIRS" \
	        -Djava.io.tmpdir="$YMIR_TMPDIR" \
	        -jar "$YMIR_HOME"/bin/server.jar $LONG_OPT "$@"
	      echo "$!" > "$_PID_FILE"
	  else
	      exec "$_RUNJAVA" $JAVA_OPTS $YMIR_OPTS \
	        -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
	        -Djava.endorsed.dirs="$ENDORSED_DIRS" \
	        -Djava.ext.dirs="$EXT_DIRS" \
	        -Djava.io.tmpdir="$YMIR_TMPDIR" \
	        -jar "$YMIR_HOME"/bin/server.jar $LONG_OPT "$@"
	      echo "$!" > "$_PID_FILE"
	  fi
  else
    touch "$YMIR_OUT"
	  if [ -f "$JAVA_AGENT_JAR" ]; then
	      $START_OS_CMD "$_RUNJAVA" $JAVA_OPTS $YMIR_OPTS \
	        -javaagent:"$JAVA_AGENT_JAR" \
	        -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
	        -Djava.endorsed.dirs="$ENDORSED_DIRS" \
	        -Djava.ext.dirs="$EXT_DIRS" \
	        -Djava.io.tmpdir="$YMIR_TMPDIR" \
	        -jar "$YMIR_HOME"/bin/server.jar $LONG_OPT "$@" \
	        >> $YMIR_OUT 2>&1 &
	        echo "$!" > "$_PID_FILE"
	        echo ""
	        echo "Ymir started in background. PID: $!"
	        if [ ! -z "$YMIR_PID" ]; then
	          echo $! > $YMIR_PID
	        fi
	  else
	      $START_OS_CMD "$_RUNJAVA" $JAVA_OPTS $YMIR_OPTS \
	        -Dorg.apache.geronimo.home.dir="$YMIR_HOME" \
	        -Djava.endorsed.dirs="$ENDORSED_DIRS" \
	        -Djava.ext.dirs="$EXT_DIRS" \
	        -Djava.io.tmpdir="$YMIR_TMPDIR" \
	        -jar "$YMIR_HOME"/bin/server.jar $LONG_OPT "$@" \
	        >> $YMIR_OUT 2>&1 &
	        echo "$!" > "$_PID_FILE"
	        echo ""
	        echo "Ymir started in background. PID: $!"
	        if [ ! -z "$YMIR_PID" ]; then
	          echo $! > $YMIR_PID
	        fi
	  fi
  fi

elif [ "$1" = "stop" ] ; then
  shift
	# check that the pid file exists
	assume_running=1
	_valid_pid
	if [ "$?" -eq "0" ] ; then
		assume_running=0
		kill "$_PID_RUN"
		echo "Shutdown requested (process #$_PID_RUN)"
		qty=0
		while [ -n "$_PID_RUN" ] && [ "$_PID" -eq "$_PID_RUN" ] ; do
			if [ "$qty" -eq "$SERVER_SHUTDOWN_TIMEOUT" ] ; then
				echo "Shutdown timeout after $SERVER_SHUTDOWN_TIMEOUT seconds, unable to stop Ymir"
				exit 1
			fi
			sleep 1
			_PID_RUN=`ps -C "$_PID" | grep -o "^$_PID"`
			qty=`expr $qty + 1`
		done
		rm "-rf" "$_PID_FILE"
	fi
	# check if the server is still running
	_jndi_connection
	if [ "$?" -eq "0" ] ; then
		echo "$SERVER_HOST:$SERVER_PORT is still responding, unable to stop Ymir"
		exit 1
	else
		if [ "$assume_running" -eq "1" ] ; then
			echo "Ymir is already stopped"
		else
			echo "Ymir successfully stopped"
		fi
		exit 0	
	fi

else

  echo "Usage: ymir.sh command [ymir_args]"
  echo "commands:"
  echo "  debug             Debug Ymir in jdb debugger"
  echo "  jpda run          Start Ymir in foreground under JPDA debugger"
  echo "  jpda start        Start Ymir in background under JPDA debugger"
  echo "  run               Start Ymir in the foreground"
  echo "  start             Start Ymir in the background"
  echo "  stop              Stop Ymir"
  echo "  stop --force      Stop Ymir (followed by kill -KILL)"
  echo ""
  echo "args for debug, jpda run, jpda start, run and start commands:"
  echo "       --quiet       No startup progress"
  echo "       --long        Long startup progress"
  echo "  -v   --verbose     INFO log level"
  echo "  -vv  --veryverbose DEBUG log level"
  echo "       --override    Override configurations. USE WITH CAUTION!"
  echo "       --help        Detailed help."
  echo "  -s   --secure      Enable Ymir for 2 way secure JMX communication."
  echo ""
  echo "args for stop command:"
  echo "       --user        Admin user"
  echo "       --password    Admin password"
  echo "       --host        Hostname of the server"
  echo "       --port        RMI port to connect to"
  echo "       --secure      Enable secure JMX communication"
  exit 1

fi
