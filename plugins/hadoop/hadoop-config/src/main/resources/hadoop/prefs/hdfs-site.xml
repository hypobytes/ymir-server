<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<!--
	Licensed to HypoBytes Ltd. under one or more contributor
	license agreements. See the NOTICE file distributed with
	this work for additional information regarding copyright
	ownership.

	HypoBytes Ltd. licenses this file to You under the
	Apache License, Version 2.0 (the "License"); you may not
	use this file except in compliance with the License.

	You may obtain a copy of the License at:

		http://www.apache.org/licenses/LICENSE-2.0
		https://hypobytes.com/licenses/APACHE-2.0

	Unless required by applicable law or agreed to in writing,
	software distributed under the License is distributed on an
	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
	KIND, either express or implied. See the License for the
	specific language governing permissions and limitations
	under the License.
	
	NOTICE: Descriptions copied and edited from 
	core-default.xml and hdfs-default.xml
	in Apache Hadoop distribution.
-->
<!-- Contains Ymir customization and important Hadoop preferences. -->
<!-- Put site-specific property overrides in this file. -->
<!-- See core-orig.xml and hdfs-orig.xml for a full list of options -->
<configuration>
	<!-- System properties -->
	<property>
		<name>fs.default.name</name>
		<value>hdfs://${PlanServerHostname}:9000</value>
		<description>
			The name of the default file system. A URI whose scheme and 
			authority determine the FileSystem implementation. 
			The uri's scheme determines the config property (fs.SCHEME.impl) 
			naming the FileSystem implementation class. The uri's authority 
			is used to determine the host, port, etc. for a filesystem.
			This is also the address and port the name node will listen on.
		</description>
	</property>
	<property>
		<name>hadoop.tmp.dir</name>
		<value>${org.apache.geronimo.home.dir}/var/tmp/hadoop</value>
		<description>
			The base directory for other temporary directories.
		</description>
	</property>
	<property>
		<name>dfs.replication</name>
		<value>1</value>
		<description>
			Default block replication. The actual number of replications 
			can be specified when the file is created. The default is used 
			if replication is not specified in create time.
		</description>
	</property>
	<property>
		<name>dfs.blocksize</name>
		<value>67108864</value>
		<description>
			The default block size for new files.
			67108864 is 64 MB.
		</description>
	</property>
	<!-- NameNode properties -->
	<property>
		<name>dfs.namenode.name.dir</name>
		<value>file://${org.apache.geronimo.home.dir}/var/config/hadoop/dfs/namenode/data</value>
		<description>
			Determines where on the local filesystem the DFS name node
			should store the name table (fsimage). If this is a comma-delimited 
			list of directories then the name table is replicated in all of the
			directories, for redundancy.
		</description>
	</property>
	<property>
		<name>dfs.namenode.edits.dir</name>
		<value>file://${org.apache.geronimo.home.dir}/var/config/hadoop/dfs/namenode/edits</value>
		<description>
			Determines where on the local filesystem the DFS name node
			should store the transaction (edits) file. If this is a
			comma-delimited list of directories then the transaction 
			file is replicated in all of the directories, for redundancy. 
			Default value is same as dfs.name.dir, but this is deprecated.
		</description>
	</property>
	<property>
		<name>dfs.namenode.http-address</name>
		<value>${PlanServerHostname}:50070</value>
		<description>
			The address and the base port where the dfs namenode web ui 
			will listen on.
			If the port is 0 then the server will start on a free port.
		</description>
	</property>
	<!-- DataNode properties -->
	<property>
		<name>dfs.datanode.data.dir</name>
		<value>file://${org.apache.geronimo.home.dir}/var/config/hadoop/dfs/datanode/data</value>
		<description>
			Determines where on the local filesystem an DFS data node
			should store its blocks. If this is a comma-delimited
			list of directories, then data will be stored in all named
			directories, typically on different devices.
			Directories that do not exist are ignored.
	  </description>
	</property>
	<property>
		<name>dfs.datanode.du.reserved</name>
		<value>1073741824</value>
		<description>
			Reserved space in bytes per volume. Always leave this much
			space free for non dfs use.
			We reserve 1 GB.
	  </description>
	</property>
	<property>
		<name>dfs.datanode.address</name>
		<value>${PlanServerHostname}:50010</value>
		<description>
			The address where the datanode server will listen to.
			If the port is 0 then the server will start on a free port.
		</description>
	</property>
	<property>
		<name>dfs.datanode.ipc.address</name>
		<value>${PlanServerHostname}:50020</value>
		<description>
			The datanode ipc server address and port.
			If the port is 0 then the server will start on a free port.
		</description>
	</property>
	<property>
		<name>dfs.datanode.http.address</name>
		<value>${PlanServerHostname}:50075</value>
		<description>
			The datanode http server address and port.
			If the port is 0 then the server will start on a free port.
		</description>
	</property>
</configuration>
