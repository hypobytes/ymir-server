/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hadoop.mapred;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.gbean.annotation.ParamSpecial;
import org.apache.geronimo.gbean.annotation.SpecialAttributeType;
import org.apache.geronimo.kernel.GBeanNotFoundException;
import org.apache.geronimo.kernel.Kernel;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.YmirJobTracker;
import org.apache.hadoop.mapreduce.server.jobtracker.State;
import org.apache.hadoop.util.VersionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;

/**
 * GBean implementation of the 
 * {@link org.apache.hadoop.mapred.JobTracker Hadoop JobTracker}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="Hadoop JobTracker")
public class JobTrackerGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(JobTrackerGBean.class);
	
	private JobConf config;
	
	private long timeout;
	
	private YmirJobTracker jobtracker;
	
	private JobTrackerRunner jobrunner;
	
	private Kernel kernel;

	/**
	 * Creates a new Hadoop JobTracker GBean instance.
	 * @param timeout The timeout in seconds to wait for the JobTracker to begin start.
	 * @param serverInfo The Geronimo server information.
	 * @param kernel The Geronimo kernel.
	 */
	public JobTrackerGBean(
			@ParamAttribute(name="timeout") final int timeout,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo,
			@ParamSpecial(type = SpecialAttributeType.kernel) final Kernel kernel) {
		super(serverInfo);
		this.timeout = timeout * 1000;
		this.kernel = kernel;
		config = new JobConf();
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Starting Hadoop JobTracker version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		try {
			jobtracker = YmirJobTracker.startTracker(config);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Unable to start Hadoop JobTracker on %s", 
					getHostname()), e);
		}
		jobrunner = new JobTrackerRunner();
		new Thread(jobrunner).start();
		
		while(jobtracker.getJobTrackerState() != State.RUNNING){
			
		}
		
		long time = System.currentTimeMillis();
		while(jobtracker.getJobTrackerState() != State.RUNNING){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				break;
			}
			long since = System.currentTimeMillis() - time;
			if(since >= timeout){
				break;
			}
		}
		if(jobtracker.getJobTrackerState() != State.RUNNING){
			throw new IllegalStateException(String.format(
					"Hadoop JobTracker did not start in %s seconds." +
					"Check your configuration or increase the timeout " +
					"(in config-substitutions.properties).",
							(timeout / 1000)));
		}
		
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping Hadoop JobTracker on %s",
					getHostname()));
		stopTaskTracker();
		if(jobrunner != null){
			try {
				jobrunner.stop();
			} catch (IOException e) {
				log.error(String.format("Unknown problem while stopping Hadoop JobTracker on %s", 
						getHostname()), e);
			}
			jobrunner = null;
		}
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try{
			doStop();
		}catch(Exception ignored){}
	}

	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The JobTracker RCP address.
	 */
	public InetSocketAddress getRPC(){
		return (jobtracker != null ? jobtracker.getInterTrackerAddress() 
				: null);
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The JobTracker HTTP address.
	 */
	public InetSocketAddress getHTTP(){
		return (jobtracker != null ? jobtracker.getInfoServerAddress() 
				: null);
	}
	
	class JobTrackerRunner implements Runnable{

		@Override
		public void run() {
			try {
				jobtracker.offerService();
			} catch (InterruptedException ignored) {
			} catch (IOException e) {
				log.error(String.format("Hadoop JobTracker caused IO error on %s", 
						getHostname()), e);
				doFail();
			}
		}
		
		public void stop() throws IOException{
			jobtracker.stopTracker();
		}
	}
	
	private void stopTaskTracker(){
		try {
			TaskTrackerGBean node = kernel.getGBean(TaskTrackerGBean.class);
			if(!node.isStopped()){
				node.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if we're in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping TaskTracker GBean", e);
			}
		}
	}
}
