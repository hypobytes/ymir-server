/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hadoop;

import java.io.File;
import java.io.FileFilter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.hdfs.server.common.HdfsConstants.StartupOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for Ymir Hadoop GBeans.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public abstract class HadoopNode {
	
	private static Logger log = LoggerFactory.getLogger(HadoopNode.class);
	
	private static FileFilter DIRS = new DirFilter();
	
	private static FileFilter FILES = new FilFilter();
	
	protected boolean stopped;
	
	/**
	 * Creates a new Hadoop node instance.
	 * @param serverInfo The server information, 
	 * used to get the installation base directory.
	 */
	protected HadoopNode(final ServerInfo serverInfo){
		stopped = true;
		// TODO: use ServerInfo to get the server root
		String logdir = System.getProperty("hadoop.log.dir");
		if(logdir == null){
			logdir = serverInfo.getBaseDirectory() + "/var/log/hadoop";
			System.setProperty("hadoop.log.dir", logdir);
		}
		File dir = new File(logdir);
		if(!dir.exists() && !dir.mkdirs()){
			log.warn(String.format("Unable to create Hadoop log directory %s", 
					dir.getPath()));
		}else if(!dir.isDirectory() && dir.canWrite()){
			log.warn(String.format("Hadoop log directory %s is not a writable directory", 
					dir.getPath()));
		}
	}
	
	/**
	 * Tells whether this Hadoop node instance is stopped or not.
	 * @return False if this Hadoop node instance is running, otherwise false.
	 */
	public boolean isStopped(){
		return stopped;
	}

	/**
	 * Parses a {@link StartupOption startup option} string.
	 * @param startupOption The user-provided startup option.
	 * @return A system representation of the user provided startup option,
	 * or {@link StartupOption#REGULAR REGULAR} if the option can not be parsed.
	 */
	protected StartupOption parseStartupOption(final String startupOption){
		// parse startup option
		StartupOption opt = StartupOption.REGULAR;
		if(startupOption != null){
			String sopt = new String(startupOption);
			if(!sopt.startsWith("-")){
				sopt = "-" + startupOption;
			}
			StartupOption userOpt = null;
			for(StartupOption so: StartupOption.values()){
				if(sopt.equalsIgnoreCase(so.getName())){
					userOpt = so;
					break;
				}
			}
			if(userOpt == null){
				log.warn(String.format("Ignoring unknown startup option %s", startupOption));
			}else{
				opt = userOpt;
			}
		}
		return opt;
	}
	
	/**
	 * Recursively deletes a directory and all its child directories and files.
	 * @param directory The directory to delete.
	 * @throws IllegalStateException If a file or directory can not be deleted.
	 */
	protected void recursiveDelete(final File directory){
		File[] dirs = directory.listFiles(DIRS);
		if(dirs != null && dirs.length > 0){
			for(File dir: dirs){
				recursiveDelete(dir);
			}
		}
		File[] files = directory.listFiles(FILES);
		if(files != null && files.length > 0){
			for(File file: files){
				if(!file.delete()){
					throw new IllegalStateException(String.format(
							"Unable to delete file %s", file.getPath()));
				}
			}
		}
		if(!directory.delete()){
			throw new IllegalStateException(String.format(
					"Unable to delete directory %s", directory.getPath()));
		}		
	}
	
	static class DirFilter implements FileFilter{

		@Override
		public boolean accept(File file) {
			return file.isDirectory();
		}
	}
	
	static class FilFilter implements FileFilter{

		@Override
		public boolean accept(File file) {
			return !file.isDirectory();
		}
	}
	
	/**
	 * Null-safe getter for the local host name.
	 * @return The {@link InetAddress#getLocalHost() local host name}, 
	 * or 'unkown' if this is not available.
	 */
	protected String getHostname(){
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "unknown";
		}
	}
	
	private static final String GBEAN_NOT_RUNNING = "GBean is not running";
	
	/**
	 * Check whether the exception indicates that a GBean is not running.
	 * @param e The exception.
	 * @return True if the exception indicates that a GBean is not running,
	 * otherwise false.
	 */
	protected boolean gbeanNotRunning(Exception e){
		return (e instanceof IllegalStateException && e.getMessage() != null && e.getMessage().startsWith(GBEAN_NOT_RUNNING));
	}
}
