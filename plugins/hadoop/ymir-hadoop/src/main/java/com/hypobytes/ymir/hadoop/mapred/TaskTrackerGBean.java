/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hadoop.mapred;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.YmirTaskTracker;
import org.apache.hadoop.mapreduce.server.tasktracker.TTConfig;
import org.apache.hadoop.net.NetUtils;
import org.apache.hadoop.util.ReflectionUtils;
import org.apache.hadoop.util.VersionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;

/**
 * GBean implementation of the 
 * {@link org.apache.hadoop.mapred.TaskTracker Hadoop TaskTracker}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="Hadoop TaskTracker")
public class TaskTrackerGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(TaskTrackerGBean.class);
	
	private JobConf config;
	
	private YmirTaskTracker tasktracker;
	
	private TaskTrackerRunner taskrunner;
	

	/**
	 * Creates a new Hadoop TaskTracker GBean instance.
	 * @param serverInfo The Geronimo server information.
	 */
	public TaskTrackerGBean(
			@ParamReference(name="serverInfo") final ServerInfo serverInfo){
		super(serverInfo);
		config = new JobConf();
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Starting Hadoop TaskTracker version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		// lifed from TaskTracker main
		// enable the server to track time spent waiting on locks
		ReflectionUtils.setContentionTracing(
				config.getBoolean(TTConfig.TT_CONTENTION_TRACKING, false));
		try {
			tasktracker = YmirTaskTracker.createInstance(config);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Unable to start Hadoop TaskTracker on %s", 
					getHostname()), e);
		}
		taskrunner = new TaskTrackerRunner();
		new Thread(taskrunner).start();
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping Hadoop TaskTracker on %s",
					getHostname()));
		if(taskrunner != null){
			try {
				taskrunner.stop();
			} catch (IOException e) {
				log.error(String.format("Unknown problem while stopping Hadoop TaskTracker on %s", 
						getHostname()), e);
			}
			taskrunner = null;
		}
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try{
			doStop();
		}catch(Exception ignored){}
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The TaskTracker RCP address.
	 */
	public InetSocketAddress getRPC(){
		return (tasktracker != null ? tasktracker.getTaskTrackerReportAddress() 
				: null);
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The TaskTracker HTTP address.
	 */
	public InetSocketAddress getHTTP(){
		return (tasktracker != null 
				? NetUtils.createSocketAddr(config.get(
						TTConfig.TT_HTTP_ADDRESS, "0.0.0.0:50060")) 
				: null);
	    
	}

	class TaskTrackerRunner implements Runnable{

		@Override
		public void run() {
			tasktracker.run();
		}
		
		public void stop() throws IOException{
			tasktracker.shutdown();
		}
	}
}
