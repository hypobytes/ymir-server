/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hadoop.datanode;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.DFSConfigKeys;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.apache.hadoop.hdfs.server.common.HdfsConstants.StartupOption;
import org.apache.hadoop.hdfs.server.datanode.DataNode;
import org.apache.hadoop.util.VersionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;

/**
 * GBean implementation of the {@link DataNode Hadoop DataNode}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="Hadoop DataNode")
public class DataNodeGBean extends HadoopNode implements GBeanLifecycle{

	private static Logger log = LoggerFactory.getLogger(DataNodeGBean.class);
	
	private DataNode datanode;
	
	private Configuration config;
	
	private StartupOption startupOption;
	
	/**
	 * Creates a new Hadoop DataNode GBean instance.
	 * @param startupOption The {@link StartupOption startup option}.
	 * @param serverInfo The Geronimo server information.
	 */
	public DataNodeGBean(
			@ParamAttribute(name="startupOption") final String startupOption,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo){
		super(serverInfo);
		
		// setup the datanode configuration
		config = new HdfsConfiguration();
		
		switch ((this.startupOption = parseStartupOption(startupOption))) {
		case REGULAR:
			break;
		case FORMAT:
			break;
		default:
			throw new UnsupportedOperationException(String.format(
					"Startup option %s not supported", this.startupOption.getName()));
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Starting Hadoop DataNode version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		if(startupOption == StartupOption.FORMAT){
			// see HDFS-107, seems deleting the data dir is the best option
			try{
				String[] dirs = config.get(DFSConfigKeys.DFS_DATANODE_DATA_DIR_KEY)
					.split(",");
				for(String dirname: dirs){
					File dir = new File(new URI(dirname));
					if(dir.exists()){
						recursiveDelete(dir);
					}
					if(!dir.mkdirs()){
						throw new IllegalStateException(String.format(
								"Unable to create DataNode directory %s", dir.getPath()));
					}
				}
			}catch (Exception e) {
				throw new IllegalStateException("Unable to format DataNode directories", e);
			}
		}
		// instantiate and start the datanode
		try {
			datanode = DataNode.createDataNode(null, config, null);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Unable to start Hadoop DataNode on %s", 
					getHostname()), e);
		}
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping Hadoop DataNode on %s",
					getHostname()));
		if(datanode != null){
			datanode.shutdown();
			datanode = null;
		}
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try {
			doStop();
		} catch (Exception ignored) {}
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The DataNode RPC address.
	 */
	public InetSocketAddress getRPC(){
		return (datanode != null ? datanode.getSelfAddr(): null);
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The DataNode IPC address.
	 */
	public InetSocketAddress getIPC(){
		return (datanode != null && datanode.ipcServer != null ? 
				datanode.ipcServer.getListenerAddress(): null);
	}

	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The DataNode HTTP address.
	 */
	public InetSocketAddress getHTTP(){
		return (datanode != null ? DataNode.getInfoAddr(config) : null);
	}
}
