/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package org.apache.hadoop.hdfs.server.datanode;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.AbstractList;
import java.util.Collection;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.DFSConfigKeys;
import org.apache.hadoop.hdfs.server.common.Util;
import org.apache.hadoop.hdfs.server.datanode.SecureDataNodeStarter.SecureResources;

/**
 * Exposes default-scoped methods of the {@link DataNode Hadoop DataNode}
 * to the {@link com.hypobytes.ymir.hadoop.datanode.DataNodeGBean Ymir DataNodeGBean}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class YmirDataNode extends DataNode {

	/**
	 * @param conf
	 * @param dataDirs
	 * @param resources
	 * @throws IOException
	 */
	public YmirDataNode(Configuration conf, AbstractList<File> dataDirs,
			SecureResources resources) throws IOException {
		super(conf, dataDirs, resources);
	}
	
	public static Collection<URI> getStorageDirs(Configuration conf) {
		Collection<String> dirNames = conf.getTrimmedStringCollection(
				DFSConfigKeys.DFS_DATANODE_DATA_DIR_KEY);
		return Util.stringCollectionAsURIs(dirNames);
	}
}
