/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package org.apache.hadoop.mapred;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.ipc.RPC.VersionMismatch;
import org.apache.hadoop.security.AccessControlException;
import org.apache.hadoop.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Exposes protected {@link JobTracker Hadoop JobTracker} functions to the
 * {@link com.hypobytes.ymir.hadoop.mapred.JobTrackerGBean Ymir JobTrackerGBean}. 
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class YmirJobTracker extends JobTracker {
	
	
	private static Logger log = LoggerFactory.getLogger(YmirJobTracker.class);

	
	/**
	 * @param conf
	 * @param newClock
	 * @param identifier
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public YmirJobTracker(JobConf conf, Clock newClock, String identifier) 
			throws IOException, InterruptedException {
		super(conf, newClock, identifier);
	}

	/**
	 * Lifted from {@link JobTracker Hadoop JobTracker} to construct custom
	 * JobTracker.
	 * @param conf
	 * @return
	 * @throws IOException
	 */
	public static YmirJobTracker startTracker(JobConf conf) 
			throws IOException {
		YmirJobTracker tracker = null;
		try {
			tracker = new YmirJobTracker(conf, DEFAULT_CLOCK,  
					new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
		} catch (InterruptedException e) {
			throw new IOException("JobTracker was interrupted during startup", e);
		}
		tracker.taskScheduler.setTaskTrackerManager(tracker);
		JobEndNotifier.startNotifier();
		/*
		while (true) {
			try {
				tracker = new YmirJobTracker(conf, DEFAULT_CLOCK,  
						new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
				tracker.taskScheduler.setTaskTrackerManager(tracker);
				break;
			} catch (VersionMismatch e) {
				throw e;
			} catch (BindException e) {
				throw e;
			} catch (UnknownHostException e) {
				throw e;
			} catch (AccessControlException ace) {
				// in case of jobtracker not having right access
				// bail out
				throw ace;
			} catch (IOException e) {
				log.warn("Error starting tracker: " + 
						StringUtils.stringifyException(e));
			}
			Thread.sleep(1000);
		}
		if (tracker != null) {
			JobEndNotifier.startNotifier();
		}
		*/
		return tracker;
	}

	/**
	 * Getter for the inter tracker address.
	 * @return The inter tracker address, or null.
	 */
	public final InetSocketAddress getInterTrackerAddress(){
		return (interTrackerServer != null ? 
				interTrackerServer.getListenerAddress() : null);
	}

	/**
	 * Getter for the info server address.
	 * @return The info server address, or null.
	 */
	public final InetSocketAddress getInfoServerAddress(){
		return (infoServer != null ? 
				new InetSocketAddress(localMachine, infoPort) : null);
	}
}
