/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package org.apache.hadoop.hdfs.server.namenode;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.server.common.HdfsConstants.NamenodeRole;

/**
 * Wrapper class to expose protected stuff from {@link NameNode Hadoop NameNode}
 * to the {@link com.hypobytes.ymir.hadoop.namenode.NameNodeGBean Ymir NameNode}. 
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class YmirNameNode extends NameNode {
	
	/**
	 * @param conf
	 * @throws IOException
	 */
	YmirNameNode(Configuration conf) throws IOException {
		this(conf, NamenodeRole.ACTIVE);
	}

	/**
	 * @param conf
	 * @param role
	 * @throws IOException
	 */
	YmirNameNode(Configuration conf, NamenodeRole role) throws IOException {
		super(conf, role);
	}
	
	public static YmirNameNode createNameNode(Configuration config) throws IOException{
		return new YmirNameNode(config);
	}
}
