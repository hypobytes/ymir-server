/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.zookeeper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.Properties;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.gbean.annotation.ParamSpecial;
import org.apache.geronimo.gbean.annotation.SpecialAttributeType;
import org.apache.geronimo.kernel.GBeanNotFoundException;
import org.apache.geronimo.kernel.Kernel;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.hdfs.server.common.HdfsConstants.StartupOption;
import org.apache.zookeeper.Version;
import org.apache.zookeeper.ZooKeeperMain;
import org.apache.zookeeper.server.NIOServerCnxn;
import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServer;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.apache.zookeeper.server.quorum.QuorumPeerMain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;
import com.hypobytes.ymir.hbase.master.HMasterGBean;

/**
 * GBean implementation of the {@link ZooKeeperMain Apache ZooKeeper}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="HBase ZooKeeper")
public class ZooKeeperGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(ZooKeeperGBean.class);
	
	public static final String ZOO_CONFIG = "zoo-site.cfg";
	
	public static final String ZOO_CONFIG_DATADIR = "dataDir";
	
	public static final String ZOO_CONFIG_DATALOGDIR = "dataLogDir";
	
	public static final String ZOO_CONFIG_CLIENTPORT = "clientPort";
	
	public static final String ZOO_CONFIG_TICKTIME = "tickTime";
	
	public static final String ZOO_DEFAULT_DATADIR = "/var/config/hbase/zookeeper/data";
	
	public static final String ZOO_DEFAULT_DATALOGDIR = "/var/config/hbase/zookeeper/log";
	
	public static final Integer ZOO_DEFAULT_CLIENTPORT = 2181;
	
	public static final Integer ZOO_DEFAULT_TICKTIME = 2000;
	
	private QuorumPeerConfig config;
	
	private StartupOption startupOption;
	
	private ZooKeeperRunner zooRunner;
	
	private QuorumPeerRunner quorumRunner;
	
	private Kernel kernel;
	
	/**
	 * Creates a new Apache ZooKeeper GBean instance.
	 * @param startupOption The {@link StartupOption startup option}.
	 * Either {@link StartupOption#REGULAR REGULAR} (default)
	 * or {@link StartupOption#FORMAT FORMAT}.
	 * @param serverInfo The Geronimo server information.
	 * @param kernel The Geronimo kernel.
	 */
	public ZooKeeperGBean(
			@ParamAttribute(name="startupOption") final String startupOption,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo,
			@ParamSpecial(type = SpecialAttributeType.kernel) final Kernel kernel) {
		super(serverInfo);
		this.kernel = kernel;
		// TODO: support other options
		switch ((this.startupOption = parseStartupOption(startupOption))) {
		case REGULAR:
			break;
		case FORMAT:
			break;
		default:
			throw new UnsupportedOperationException(String.format(
					"Startup option %s not supported", this.startupOption.getName()));
		}
		// initilize the config
		Properties props = new Properties();
		URL url = Thread.currentThread().getContextClassLoader().getResource(ZOO_CONFIG);
		if(url != null){
			if(log.isInfoEnabled())
				log.info(String.format("Loading ZooKeeper configuration from %s", url.toString()));
			try{
				FileInputStream in = new FileInputStream(new File(url.toURI()));
				props.load(in);
			}catch (Exception e) {
				log.error(String.format("Error while loading ZooKeeper configuration from %s", url.toString()), e);
			}
		}else{
			if(log.isInfoEnabled())
				log.info(String.format("No ZooKeeper configuration (%s) on classpath, applying defaults", ZOO_CONFIG));
		}
		if(!props.containsKey(ZOO_CONFIG_DATADIR)){
			props.put(ZOO_CONFIG_DATADIR, serverInfo.getBaseDirectory() + ZOO_DEFAULT_DATADIR);
		}
		if(!props.containsKey(ZOO_CONFIG_DATALOGDIR)){
			props.put(ZOO_CONFIG_DATALOGDIR, serverInfo.getBaseDirectory() + ZOO_DEFAULT_DATALOGDIR);
		}
		if(!props.containsKey(ZOO_CONFIG_CLIENTPORT)){
			props.put(ZOO_CONFIG_CLIENTPORT, ZOO_DEFAULT_CLIENTPORT);
		}
		if(!props.containsKey(ZOO_CONFIG_TICKTIME)){
			props.put(ZOO_CONFIG_TICKTIME, ZOO_DEFAULT_TICKTIME);
		}
		config = new QuorumPeerConfig();
		try {
			config.parseProperties(props);
		} catch (Exception e) {
			log.error("Unable to parse ZooKeeper configuration", e);
			throw new IllegalStateException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(config.getServers().size() > 0){
			// run a quorum
			if(log.isInfoEnabled())
				log.info(String.format("Starting ZooKeeper QuorumPeer version %s on %s",
						Version.getVersion(),
						getHostname()));
			format();
			quorumRunner = new QuorumPeerRunner();
			new Thread(quorumRunner).start();
		}else{
			// run standalone
			if(log.isInfoEnabled())
				log.info(String.format("Starting ZooKeeper Server version %s on %s",
						Version.getVersion(),
						getHostname()));
			format();
			zooRunner = new ZooKeeperRunner(config);
			new Thread(zooRunner).start();
		}
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled()){
			if(config.getServers().size() > 0){
				log.info(String.format("Stopping ZooKeeper QuorumPeer on %s",
						getHostname()));
			}else{
				log.info(String.format("Stopping ZooKeeper Server on %s",
						getHostname()));
			}
		}
		stopHBase();
		if(zooRunner != null){
			zooRunner.stop();
			zooRunner = null;
		}
		if(quorumRunner != null){
			quorumRunner.stop();
			quorumRunner = null;
		}
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try{
			doStop();
		}catch (Exception ignored) {}
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The ZooKeeper listen address.
	 */
	public InetSocketAddress getListenAddress(){
		return config.getClientPortAddress();
	}

	class ZooKeeperRunner implements Runnable{
		
		private ServerConfig config;
		
		private NIOServerCnxn.Factory cnxnFactory;
		
		public ZooKeeperRunner(QuorumPeerConfig config){
			this.config = new ServerConfig();
			this.config.readFrom(config);			
		}

		@Override
		public void run() {
			try {
				runFromConfig(config);
			} catch (IOException e) {
				log.error("Unable to run ZooKeeper Server", e);
				doFail();
			}
		}
		
	    /**
	     * Lifted from {@link ZooKeeperServerMain#runFromConfig(ServerConfig) ZooKeeperServerMain}
	     * to avoid concurrency problem during shutdown.
	     * @see org.apache.zookeeper.server.ZooKeeperServerMain#runFromConfig(org.apache.zookeeper.server.ServerConfig)
	     */
	    public void runFromConfig(ServerConfig config) throws IOException {
	        try {
	            // Note that this thread isn't going to be doing anything else,
	            // so rather than spawning another thread, we will just call
	            // run() in this thread.
	            // create a file logger url from the command line args
	            ZooKeeperServer zkServer = new ZooKeeperServer();
	            FileTxnSnapLog ftxn = new FileTxnSnapLog(new
	                   File(config.getDataLogDir()), new File(config.getDataDir()));
	            zkServer.setTxnLogFactory(ftxn);
	            zkServer.setTickTime(config.getTickTime());
	            zkServer.setMinSessionTimeout(config.getMinSessionTimeout());
	            zkServer.setMaxSessionTimeout(config.getMaxSessionTimeout());
	            cnxnFactory = new NIOServerCnxn.Factory(config.getClientPortAddress(),
	                    config.getMaxClientCnxns());
	            cnxnFactory.startup(zkServer);
	            cnxnFactory.join();
	        } catch (InterruptedException e) {
	            // this should not happen
	            log.warn("ZooKeeper Server interrupted", e);
	        }
	    }
		
		public void stop(){
			cnxnFactory.shutdown();
		}
	}
	
	class QuorumPeerRunner extends QuorumPeerMain implements Runnable{
		
		@Override
		public void run() {
			System.setProperty("zookeeper.jmx.log4j.disable", "true");
			try {
				runFromConfig(config);
			} catch (IOException e) {
				log.error("Unable to run ZooKeeper QuorumPeer", e);
				doFail();
			}
		}
		
		public void stop(){
			quorumPeer.shutdown();
		}
	}
	
	private void stopHBase(){
		try {
			HMasterGBean master = kernel.getGBean(HMasterGBean.class);
			if(!master.isStopped()){
				master.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if we're in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping HMaster GBean", e);
			}
		}
	}
	
	private void format(){
		if(startupOption == StartupOption.FORMAT){
			try{
				String[] dirs = {config.getDataDir(), config.getDataLogDir()};
				for(String dirname: dirs){
//					File dir = new File(new URI(dirname));
					File dir = new File(dirname);
					if(dir.exists()){
						recursiveDelete(dir);
					}
					if(!dir.mkdirs()){
						throw new IllegalStateException(String.format(
								"Unable to create ZooKeeper directory %s", dir.getPath()));
					}
				}
			}catch (Exception e) {
				throw new IllegalStateException("Unable to format ZooKeeper directories", e);
			}
		}
	}
}
