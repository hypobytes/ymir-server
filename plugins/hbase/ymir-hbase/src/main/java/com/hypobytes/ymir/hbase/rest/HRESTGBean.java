/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.rest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.logging.LogManager;

import javax.naming.NamingException;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.gbean.annotation.ParamSpecial;
import org.apache.geronimo.gbean.annotation.SpecialAttributeType;
import org.apache.geronimo.kernel.Kernel;
import org.apache.geronimo.naming.enc.EnterpriseNamingContext;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.rest.RESTServlet;
import org.apache.hadoop.hbase.rest.ResourceConfig;
import org.apache.hadoop.hbase.util.VersionInfo;
import org.mortbay.jetty.AbstractConnector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.hypobytes.ymir.hadoop.HadoopNode;
import com.sun.jersey.spi.container.servlet.ServletContainer;

/**
 * GBean implementation of the 
 * {@link org.apache.hadoop.hbase.rest.Main HBase Rest Server}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="HBase REST Server")
public class HRESTGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(HRESTGBean.class);
	
	private Configuration config;
	
	private Server server;
	
	private AbstractConnector connector;
	
	private Context context; 
	
	private int port;

	/**
	 * Creates a new HBase REST HTTP Server.
	 * @param readOnly Tells whether the server is in read-only mode or not.
	 * @param port The listen port.
	 * @param address The listen address.
	 * @param acceptorThreads The number of acceptor threads.
	 * @param queueSize The accept queue size.
	 * @param serverInfo The Geronimo server information.
	 * @param kernel The Geronimo kernel.
	 */
	// TODO Use the Geronimo thread pool 
	public HRESTGBean(
			@ParamAttribute(name="readOnly") final boolean readOnly,
			@ParamAttribute(name="port") final int port,
			@ParamAttribute(name="address") final String address,
			@ParamAttribute(name="acceptorThreads") final int acceptorThreads,
			@ParamAttribute(name="queueSize") final int queueSize,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo,
			@ParamSpecial(type = SpecialAttributeType.kernel) final Kernel kernel) {
		super(serverInfo);
		
		config = HBaseConfiguration.create();
		config.setInt("hbase.rest.port", port);
		config.setBoolean("hbase.rest.readonly", readOnly);
//		try {
//			RESTServlet.getInstance(config);
//		} catch (IOException e) {
//			throw new IllegalStateException("Unable to create REST Servlet instance", e);
//		}
		
		this.port = port;
		
		server = new Server();
		server.setSendDateHeader(false);
		server.setSendServerVersion(false);
		// not setting this prevents the thread from running
		//server.setStopAtShutdown(false);
		
		connector = new SelectChannelConnector();
		connector.setHost(address);
		connector.setPort(port);
		connector.setAcceptors(acceptorThreads);
		connector.setAcceptQueueSize(queueSize);
		
		ServletHolder servlet = new ServletHolder(ServletContainer.class);
		servlet.setInitParameter("com.sun.jersey.config.property.resourceConfigClass",
	    		ResourceConfig.class.getCanonicalName());
	    servlet.setInitParameter("com.sun.jersey.config.property.packages", "jetty");
	    
	    // we need a custom context for java:comp jndi lookup performed by jersey
	    javax.naming.Context jndi = null;
	    try {
			jndi = EnterpriseNamingContext.createEnterpriseNamingContext(new HashMap<String, Object>(), null, kernel, this.getClass().getClassLoader());
		} catch (NamingException e) {
			throw new IllegalStateException("Unable to create JNDI context");
		}
		//context.setHandler(new JNDIHandler(jndi, context.getHandler()));
//		context.setHandler(new JNDIHandler(jndi));
	    
	    context = new Context(server, "/", new JNDIHandler(jndi), null, null, null);
	    context.addServlet(servlet, "/*");
	    // this uses Handler.DEFAULT in HBase, but the filter does not work on redirects
	    context.addFilter(org.apache.hadoop.hbase.rest.filter.GzipFilter.class, "/*", 
	    		org.mortbay.jetty.Handler.DEFAULT);
	    //context.addFilter(GzipFilter.class, "/*", 0);
		
		server.addHandler(context);
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled()){
			log.info(String.format("Starting HBase REST HTTP Server version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		}
		trapLogger();
		try {
			RESTServlet.getInstance(config);
		} catch (IOException e) {
			throw new IllegalStateException("Unable to create REST Servlet instance", e);
		}
		try {
			server.start();
			server.addConnector(connector);
			connector.start();
//			server.addHandler(context);
//			context.start();
		} catch (Exception e) {
			throw new IllegalStateException(String.format(
					"Unable to start HBase REST HTTP Server on %s", getHostname()), e);
		}
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping HBase REST HTTP Server on %s",
					getHostname()));
//		if(context != null && context.isStarted()){
//			if(server != null){
//				server.removeHandler(context);
//			}
//			try {
//				context.stop();
//			} catch (Exception e) {
//				log.error(String.format("Unable to gracefully close HBase REST HTTP " +
//						"Context %s", context.getContextPath()), e);
//			}
//		}
		if(connector != null && connector.isStarted()){
			try {
				if(server != null){
					server.removeConnector(connector);
				}
				connector.stop();
			} catch (Exception e) {
				log.error(String.format("Unable to gracefully close HBase REST HTTP " +
						"Connector on port %s", port), e);
			}
		}
		if(server != null && server.isStarted()){
			try {
				server.stop();
			} catch (Exception e) {
				log.error(String.format("Unable to gracefully stop HBase REST HTTP " +
						"Server on %s", getHostname()), e);
			}
		}
		RESTServlet.stop();
		HConnectionManager.deleteConnection(config, true);
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try {
			doStop();
		} catch (Exception ignored) {}
	}

	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The HBase REST HTTP listen address.
	 */
	public InetSocketAddress getListenAddress(){
		return (connector != null 
				? new InetSocketAddress(connector.getHost(), connector.getPort()) 
				: null);
	}
	
	/**
	 * Jersey uses the java.util.logging framework
	 */
	private void trapLogger(){
		java.util.logging.Logger rootLogger = LogManager.getLogManager().getLogger("");  
		java.util.logging.Handler[] handlers = rootLogger.getHandlers();  
		for (int i = 0; i < handlers.length; i++) {  
		    rootLogger.removeHandler(handlers[i]);  
		}  
		SLF4JBridgeHandler.install();  
	}
}
