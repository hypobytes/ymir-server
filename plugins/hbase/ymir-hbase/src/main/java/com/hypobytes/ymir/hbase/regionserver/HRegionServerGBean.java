/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.regionserver;

import java.net.InetSocketAddress;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.regionserver.HRegionServer;
import org.apache.hadoop.hbase.regionserver.YmirHRegionServer;
import org.apache.hadoop.hbase.util.VersionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;

/**
 * GBean implementation of the {@link HRegionServer HBase HRegionServer}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="HBase Region")
public class HRegionServerGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(HRegionServerGBean.class);
	
	private Configuration config;
	
	private Thread regionThread;
	
	private HRegionServer regionServer;
	
	private long timeout;
	
	/**
	 * Creates a new {@link HRegionServer HRegionServer} GBean.
	 * @param timeout The timeout in seconds to wait for the HRegionServer to
	 * {@link HRegionServer#isOnline() come online}.
	 */
	public HRegionServerGBean(
			@ParamAttribute(name="timeout") final int timeout,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo){
		super(serverInfo);
		this.timeout = timeout * 1000;
		config = HBaseConfiguration.create();
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() throws Exception {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Starting HBase HRegionServer version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		regionServer = HRegionServer.constructRegionServer(HRegionServer.class, config);
		//HRegionServer.startRegionServer(regionServer);
		regionThread = new Thread(regionServer, "HRegionServer");
		regionThread.start();
		long time = System.currentTimeMillis();
		while(!regionServer.isOnline()){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				break;
			}
			long since = System.currentTimeMillis() - time;
			if(since >= timeout){
				break;
			}
		}
		if(!regionServer.isOnline()){
			throw new IllegalStateException(String.format(
					"HBase RegionServer not online in %s seconds. " +
					"Check your configuration or increase the timeout " +
					"(in config-substitutions.properties).", 
							(timeout / 1000)));
		}
		stopped = false;
		// This adds a shutdown hook which we don't want with Ymir
		//HRegionServer.startRegionServer(regionServer);
		//master.join();
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() throws Exception {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping HBase HRegionServer on %s",
					getHostname()));
		if(regionServer != null){
			Thread stop = YmirHRegionServer.shutdownThread(config, null, regionServer, regionThread);
			stop.start();
			long time = System.currentTimeMillis();
			while(stop.isAlive()){
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					break;
				}
				long since = System.currentTimeMillis() - time;
				if(since >= timeout){
					break;
				}
			}
			if(stop.isAlive() || !regionServer.isStopped()){
				log.warn(String.format("Unable to gracefully stop HBase RegionServer on %s", 
						getHostname()));
			}
//			while(stop.isAlive()){
//				Thread.sleep(10);
//			}
			// TODO: this does not stop the filesystem cleanly
			//regionServer.stop("Stopping");
			regionServer = null;
		}
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try {
			doStop();
		} catch (Exception ignored) {}
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The HRegionServer RCP listen address.
	 */
	public InetSocketAddress getRPC(){
		return (regionServer != null ? regionServer.getServerInfo().getServerAddress().getInetSocketAddress() : null);
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The HRegionServer RCP listen address.
	 */
	public InetSocketAddress getHTTP(){
		int port = config.getInt("hbase.regionserver.info.port", 60030);
		if (port >= 0) {
			return new InetSocketAddress(config.get("hbase.regionserver.info.bindAddress", "0.0.0.0"), port);
		}
		return null;
	}
}
