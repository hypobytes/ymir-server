/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.master;

import java.net.InetSocketAddress;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.gbean.annotation.ParamSpecial;
import org.apache.geronimo.gbean.annotation.SpecialAttributeType;
import org.apache.geronimo.kernel.GBeanNotFoundException;
import org.apache.geronimo.kernel.Kernel;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.master.HMaster;
import org.apache.hadoop.hbase.util.VersionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;
import com.hypobytes.ymir.hbase.regionserver.HRegionServerGBean;
import com.hypobytes.ymir.hbase.rest.HRESTGBean;
import com.hypobytes.ymir.hbase.thrift.HThriftGBean;

/**
 * GBean implementation of the {@link HMaster HBase HMaster}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="HBase HMaster")
public class HMasterGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(HMasterGBean.class);
	
	private Configuration config;
	
	private HMaster master;
	
	private Kernel kernel;
	
	private long timeout;
	
	/**
	 * Creates a new {@link HMaster HMaster} GBean.
	 * @param timeout The timeout in seconds to wait for the HMaster to begin 
	 * {@link HMaster#isAcceptingRegionServers() accepting region server registrations}.
	 * @param serverInfo The Geronimo server information.
	 * @param kernel The Geronimo kernel.
	 */
	public HMasterGBean(
			@ParamAttribute(name="timeout") final int timeout,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo,
			@ParamSpecial(type = SpecialAttributeType.kernel) final Kernel kernel){
		super(serverInfo);
		this.timeout = timeout * 1000;
		this.kernel = kernel;
		config = HBaseConfiguration.create();
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Starting HBase HMaster version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		master = HMaster.constructMaster(HMaster.class, config);
		master.start();
		
		long time = System.currentTimeMillis();
		while(!master.isAcceptingRegionServers()){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				break;
			}
			long since = System.currentTimeMillis() - time;
			if(since >= timeout){
				break;
			}
		}
		if(!master.isAcceptingRegionServers()){
			throw new IllegalStateException(String.format(
					"HBase Master not accepting region server registrations in %s seconds." +
					"Check your configuration or increase the timeout " +
					"(in config-substitutions.properties).",
							(timeout / 1000)));
		}
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping HBase HMaster on %s",
					getHostname()));
		stopThriftServer();
		stopRESTServer();
		stopRegionServer();
		if(master != null){
			//master.stopMaster();
			master.shutdown();
			long time = System.currentTimeMillis();
			while(!master.isFullyStopped()){
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					break;
				}
				long since = System.currentTimeMillis() - time;
				if(since >= timeout){
					break;
				}
			}
			if(!master.isFullyStopped()){
				log.warn(String.format("Unable to gracefully stop HBase HMaster on %s", 
						getHostname()));
			}
			master = null;
		}
		// close all connections before proceeding
		//HConnectionManager.deleteAllConnections(false);
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try {
			doStop();
		} catch (Exception ignored) {}
	}

	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The HMaster RCP listen address.
	 */
	public InetSocketAddress getRPC(){
		return (master != null ? master.getMasterAddress().getInetSocketAddress() : null);
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The HMaster RCP listen address.
	 */
	public InetSocketAddress getHTTP(){
		int port = config.getInt("hbase.master.info.port", 60010);
		if (port >= 0) {
			return new InetSocketAddress(config.get("hbase.master.info.bindAddress", "0.0.0.0"), port);
		}
		return null;
	}
	
	private void stopRegionServer(){
		try {
			HRegionServerGBean server = kernel.getGBean(HRegionServerGBean.class);
			if(!server.isStopped()){
				server.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if running in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping HRegionServer GBean", e);
			}
		}	
	}
	
	private void stopRESTServer(){
		try {
			HRESTGBean server = kernel.getGBean(HRESTGBean.class);
			if(!server.isStopped()){
				server.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if running in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping HREST GBean", e);
			}
		}
	}
	
	private void stopThriftServer(){
		try {
			HThriftGBean server = kernel.getGBean(HThriftGBean.class);
			if(!server.isStopped()){
				server.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if running in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping HThrift GBean", e);
			}
		}
	}
}
