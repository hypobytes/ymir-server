/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.rest;

import java.io.IOException;

import javax.naming.Context;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.geronimo.naming.java.RootContext;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.servlet.SessionHandler;

/**
 * Handler that sets the correct JNDI context.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class JNDIHandler extends SessionHandler implements Handler {
	
	private Context context;
	
//	private Handler next;
	
	/**
	 * Creates a new JNDI handler.
	 * @param context The JNDI context to use for requests.
	 * @param next The next handler in the chain, 
	 * invoked after setting the JNDI context.
	 */
	public JNDIHandler(Context context){
		this.context = context;
//		this.next = next;
	}

	/* (non-Javadoc)
	 * @see org.mortbay.jetty.Handler#handle(java.lang.String, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, int)
	 */
	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, int dispatch) throws IOException,
			ServletException {
        Context oldContext = RootContext.getComponentContext();
        try {
            RootContext.setComponentContext(context);
            super.handle(target, request, response, dispatch);
            //next.handle(target, request, response, dispatch);
        } finally {
            RootContext.setComponentContext(oldContext);
        }
	}
}
