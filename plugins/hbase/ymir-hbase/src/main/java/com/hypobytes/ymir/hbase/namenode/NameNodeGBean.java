/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.namenode;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.gbean.annotation.ParamSpecial;
import org.apache.geronimo.gbean.annotation.SpecialAttributeType;
import org.apache.geronimo.kernel.GBeanNotFoundException;
import org.apache.geronimo.kernel.Kernel;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.server.common.HdfsConstants.StartupOption;
import org.apache.hadoop.hdfs.server.namenode.NameNode;
import org.apache.hadoop.hdfs.server.namenode.YmirHBaseNameNode;
import org.apache.hadoop.util.VersionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;
import com.hypobytes.ymir.hbase.datanode.DataNodeGBean;
import com.hypobytes.ymir.hbase.master.HMasterGBean;

/**
 * GBean implementation of the {@link NameNode Hadoop NameNode}.
 * 
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="HBase NameNode")
public class NameNodeGBean extends HadoopNode implements GBeanLifecycle {

	private static Logger log = LoggerFactory.getLogger(NameNodeGBean.class);
	
	private Configuration config;
	
	private StartupOption startupOption;
	
	private YmirHBaseNameNode namenode;
	
	private Kernel kernel;
	
	/**
	 * Creates a new Hadoop HBase GBean instance.
	 * @param startupOption The {@link StartupOption startup option}.
	 * @param serverInfo The Geronimo server information.
	 * @param kernel The Geronimo kernel.
	 */
	public NameNodeGBean(
			@ParamAttribute(name="startupOption") final String startupOption,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo,
			@ParamSpecial(type = SpecialAttributeType.kernel) final Kernel kernel){
		super(serverInfo);
	    
		config = new Configuration();
		
		// TODO: support other options
		switch ((this.startupOption = parseStartupOption(startupOption))) {
		case REGULAR:
			break;
		case FORMAT:
			break;
		default:
			throw new UnsupportedOperationException(String.format(
					"Startup option %s not supported", this.startupOption.getName()));
		}
		
		this.kernel = kernel;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled()){
			log.info(String.format("Starting HBase NameNode version %s on %s",
					VersionInfo.getVersion(),
					getHostname()));
		}
		config.set("dfs.namenode.startup", startupOption.toString());
		try {
			namenode = YmirHBaseNameNode.createNameNode(config);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Unable to start HBase NameNode on %s", 
					getHostname()), e);
		}
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping HBase NameNode on %s",
					getHostname()));
		stopHBase();
		stopDataNode();
		if(namenode != null){
			namenode.stop();
			namenode = null;
		}
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try {
			doStop();
		} catch (Exception ignored) {}
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The NameNode RCP listen address.
	 */
	public InetSocketAddress getRPC(){
		return (namenode != null ? namenode.getNameNodeAddress() : null);
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The NameNode HTTP listen address.
	 */
	public InetSocketAddress getHTTP(){
		return (namenode != null ? namenode.getHttpAddress() : null);
	}
	
	private void stopHBase(){
		try {
			HMasterGBean master = kernel.getGBean(HMasterGBean.class);
			if(!master.isStopped()){
				master.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if we're in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping HMaster GBean", e);
			}
		}
	}
	
	private void stopDataNode(){
		try {
			DataNodeGBean node = kernel.getGBean(DataNodeGBean.class);
			if(!node.isStopped()){
				node.doStop();
			}
		} catch (GBeanNotFoundException ignored) {
			// ignore this if we're in standalone mode
		} catch (Exception e) {
			// ignore gbean not running
			if(!gbeanNotRunning(e)){
				log.error("Unknown problem while stopping DataNode GBean", e);
			}
		}
	}
}
