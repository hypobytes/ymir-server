/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.thrift;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.geronimo.gbean.GBeanLifecycle;
import org.apache.geronimo.gbean.annotation.GBean;
import org.apache.geronimo.gbean.annotation.ParamAttribute;
import org.apache.geronimo.gbean.annotation.ParamReference;
import org.apache.geronimo.system.serverinfo.ServerInfo;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.thrift.ThriftServer.HBaseHandler;
import org.apache.hadoop.hbase.thrift.YmirThriftServer;
import org.apache.hadoop.hbase.thrift.generated.Hbase;
import org.apache.hadoop.hbase.util.VersionInfo;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TTransportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hypobytes.ymir.hadoop.HadoopNode;

/**
 * GBean implementation of the 
 * {@link org.apache.hadoop.hbase.thrift.ThriftServer HBase Thrift Server}.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
@GBean(name="HBase Thrift Server")
public class HThriftGBean extends HadoopNode implements GBeanLifecycle {
	
	private static Logger log = LoggerFactory.getLogger(HThriftGBean.class);
	
	private Configuration config;
	
	private ThriftMode tmode;
	
	private String address;
	
	private int port;
	
	private long timeout;
	
	private TServer server;
	
	private TServerRunner trunner;
	
	/**
	 * Creates a new HBase Thrift Server.
	 * @param mode The server move, THREADPOOL (default), NONBLOCKING or HSHA.
	 * @param address The listen address, only used with THREADPOOL mode.
	 * @param port The listen port.
	 * @param framed Use framed transport?
	 * @param compact Use compact protocol?
	 * @param timeout Server start/stop timeout, in seconds.
	 * @param serverInfo The Geronimo server information.
	 */
	public HThriftGBean(
			@ParamAttribute(name="mode") final String mode,
			@ParamAttribute(name="address") final String address,
			@ParamAttribute(name="port") final int port,
			@ParamAttribute(name="framed") final boolean framed,
			@ParamAttribute(name="compact") final boolean compact,
			@ParamAttribute(name="timeout") final int timeout,
			@ParamReference(name="serverInfo") final ServerInfo serverInfo){
		super(serverInfo);
		
		this.address = address;
		this.port = port;
		this.timeout = timeout * 1000;
		
		tmode = ThriftMode.fromInput(mode);
		config = HBaseConfiguration.create();
		
		HBaseHandler handler;
	    try {
			handler = YmirThriftServer.handler(config);
		} catch (IOException e) {
			throw new IllegalStateException("Unable to create HBase Thrift Handler", e);
		}
	    Hbase.Processor processor = new Hbase.Processor(handler);
	    
	    TProtocolFactory pfactory = (compact 
	    		? new TCompactProtocol.Factory() 
	    		: new TBinaryProtocol.Factory());
	    
	    if(tmode == ThriftMode.THREADPOOL){
	    	TServerTransport transport;
			try {
				transport = new TServerSocket(new InetSocketAddress(address, port));
			} catch (TTransportException e) {
				throw new IllegalStateException(String.format(
						"Unable to create TServerSocket on address %s port %s", 
						address, port), e);
			}

	    	TTransportFactory factory = (framed 
	    			? new TFramedTransport.Factory() 
	    			: new TTransportFactory());
	    	server = new TThreadPoolServer(processor, transport, factory, pfactory);	    	
	    }else{
	    	TNonblockingServerTransport transport;
			try {
				transport = new TNonblockingServerSocket(port);
			} catch (TTransportException e) {
				throw new IllegalStateException(String.format(
						"Unable to create TNonblockingServerSocket on port %s", 
						port), e);
			}
	    	TFramedTransport.Factory factory = new TFramedTransport.Factory();

	    	server = (tmode == ThriftMode.NONBLOCKING 
	    			? new TNonblockingServer(processor, transport, factory, pfactory)
	    			: new THsHaServer(processor, transport, factory, pfactory));
	    }
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStart()
	 */
	@Override
	public void doStart() {
		if(!stopped){
			return;
		}
		if(log.isInfoEnabled()){
			log.info(String.format("Starting HBase %s Thrift Server version %s on %s",
					tmode.toString(),
					VersionInfo.getVersion(),
					getHostname()));
		}
		trunner = new TServerRunner();
		new Thread(trunner).start();
		// TODO find a way to check if the server has started
		stopped = false;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doStop()
	 */
	@Override
	public void doStop() {
		if(stopped){
			return;
		}
		if(log.isInfoEnabled())
			log.info(String.format("Stopping HBase %s Thrift Server on %s",
					tmode.toString(),
					getHostname()));
		if(server != null){
			server.stop();
			long time = System.currentTimeMillis();
			while(trunner != null && !trunner.stopped){
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					break;
				}
				long since = System.currentTimeMillis() - time;
				if(since >= timeout){
					break;
				}
			}
			if(trunner != null && !trunner.stopped){
				throw new IllegalStateException(String.format(
						"HBase Thrift Server did not stop in %s seconds." +
						"Check your configuration or increase the timeout " +
						"(in config-substitutions.properties).",
								(timeout / 1000)));
			}
		}
		HConnectionManager.deleteConnection(config, true);
		stopped = true;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.gbean.GBeanLifecycle#doFail()
	 */
	@Override
	public void doFail() {
		try {
			doStop();
		} catch (Exception ignored) {}
	}
	
	/**
	 * Makes Geronimo print a nice listen message on startup.
	 * @return The HThrift listen address.
	 */
	public InetSocketAddress getListenAddress(){
		InetSocketAddress adr;
		if(tmode == ThriftMode.THREADPOOL){
			adr = new InetSocketAddress(address, port);
		}else{
			adr = new InetSocketAddress(port);
		}
		return adr;
	}
	
	enum ThriftMode{
		
		THREADPOOL,HSHA,NONBLOCKING;
		
		static ThriftMode fromInput(String input){
			if(input == null || input.equalsIgnoreCase("threadpool")){
				return THREADPOOL;
			}
			if(input.equalsIgnoreCase("hsha")){
				return HSHA;
			}
			if(input.equalsIgnoreCase("nonblocking")){
				return NONBLOCKING;
			}
			log.warn(String.format(
					"Ignoring unknown HBase Thrift mode %s and using threadpool", input));
			return THREADPOOL;
		}

		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			switch (this) {
			case THREADPOOL:
				return "ThreadPool";
			case HSHA:
				return "HsHA";
			case NONBLOCKING:
				return "NonBlocking";
			default:
				return super.toString();
			}
		}
	}
	
	class TServerRunner implements Runnable{
		
		boolean stopped = true;
		
		boolean stop = false;

		@Override
		public void run() {
			stopped = false;
			try{
				server.serve();
			}catch (Exception e) {
				log.error(String.format(
						"Unknown problem when starting TServer on %s", 
						getHostname()), e);
				doFail();
			}finally{
				stopped = true;
			}
		}
	}
}
