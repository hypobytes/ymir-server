/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.test;


import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Basic tests of Ymir HBase BigTable functionality.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class BigTableIT {

	/**
	 * Tests HBase connection.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Connection Test")
	public void hbaseConnectionTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertNotNull(admin);
		Assert.assertTrue(admin.isMasterRunning());
	}
	
	/**
	 * Tests HBase table creation.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Table Creation Test", dependsOnMethods={"hbaseConnectionTest"})
	public void hbaseTableCreationTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertTrue(admin.isMasterRunning());
		HTableDescriptor desc = new HTableDescriptor("table_creation_test");
		Assert.assertFalse(admin.tableExists(desc.getName()));
		admin.createTable(desc);
		Assert.assertTrue(admin.tableExists(desc.getName()));
		Assert.assertTrue(admin.isTableAvailable(desc.getName()));
		Assert.assertTrue(admin.isTableEnabled(desc.getName()));
		HTable table = new HTable(config, desc.getName());
		Assert.assertNotNull(table);
	}
	
	/**
	 * Tests HBase column creation.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Column Creation Test", dependsOnMethods={"hbaseTableCreationTest"})
	public void hbaseColumnCreationTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertTrue(admin.isMasterRunning());
		HTableDescriptor desc = new HTableDescriptor("table_creation_test");
		Assert.assertTrue(admin.tableExists(desc.getName()));
		Assert.assertTrue(admin.isTableEnabled(desc.getName()));
		admin.disableTable(desc.getName());
		Assert.assertFalse(admin.isTableEnabled(desc.getName()));
		HColumnDescriptor col = new HColumnDescriptor("default_family");
		admin.addColumn(desc.getName(), col);
		admin.enableTable(desc.getName());
		Assert.assertTrue(admin.isTableEnabled(desc.getName()));
	}
	
	/**
	 * Tests HBase put.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Put Test", dependsOnMethods={"hbaseColumnCreationTest"})
	public void hbasePutTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertTrue(admin.isMasterRunning());
		HTable table = new HTable(config, "table_creation_test");
		Put put = new Put(Bytes.toBytes("put_row"));
		put.add(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"), Bytes.toBytes("value"));
		table.put(put);
	}
	
	/**
	 * Tests HBase get.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Get Test", dependsOnMethods={"hbasePutTest"})
	public void hbaseGetTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertTrue(admin.isMasterRunning());
		HTable table = new HTable(config, "table_creation_test");
		
		Get get = new Get(Bytes.toBytes("put_row"));
		Result res = table.get(get);
		Assert.assertFalse(res.isEmpty());
		Assert.assertNotNull(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier")));
		Assert.assertEquals(Bytes.toString(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"))), "value");
	}
	
	/**
	 * Tests HBase scan.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Scan Test", dependsOnMethods={"hbaseGetTest"})
	public void hbaseScanTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertTrue(admin.isMasterRunning());
		HTable table = new HTable(config, "table_creation_test");
		
		Put put = new Put(Bytes.toBytes("scan_row"));
		put.add(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"), Bytes.toBytes("scan"));
		table.put(put);
		
		Result res;
		Scan scan = new Scan();
		scan.addColumn(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"));
		ResultScanner rs = table.getScanner(scan);
		Iterator<Result> i = rs.iterator();
		Assert.assertTrue(i.hasNext());
		res = i.next();
		Assert.assertFalse(res.isEmpty());
		Assert.assertNotNull(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier")));
		Assert.assertEquals(Bytes.toString(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"))), "value");
		Assert.assertTrue(i.hasNext());
		res = i.next();
		Assert.assertFalse(res.isEmpty());
		Assert.assertNotNull(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier")));
		Assert.assertEquals(Bytes.toString(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"))), "scan");
		Assert.assertFalse(i.hasNext());
	}
	
	/**
	 * Tests HBase table deletion.
	 * @throws Exception Upon any error.
	 */
	@Test(groups={"hbase"}, testName="HBase Table Deletion Test", dependsOnMethods={"hbaseScanTest"})
	public void hbaseTableDeletionTest() throws Exception{
		Configuration config = HBaseConfiguration.create();
		HBaseAdmin admin = new HBaseAdmin(config);
		Assert.assertTrue(admin.isMasterRunning());
		HTableDescriptor desc = new HTableDescriptor("table_creation_test");
		Assert.assertTrue(admin.tableExists(desc.getName()));
		admin.disableTable(desc.getName());
		admin.deleteTable(desc.getName());
		Assert.assertFalse(admin.tableExists(desc.getName()));
	}
	
	public static void main(String[] args) throws Exception{
		BigTableIT it = new BigTableIT();
		it.hbaseConnectionTest();
		it.hbaseTableCreationTest();
		it.hbaseColumnCreationTest();
		it.hbasePutTest();
		it.hbaseGetTest();
		it.hbaseScanTest();
		it.hbaseTableDeletionTest();
	}
}
