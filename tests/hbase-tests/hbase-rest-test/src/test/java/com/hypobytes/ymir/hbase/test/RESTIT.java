/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hbase.test;

import java.net.URI;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.rest.client.Client;
import org.apache.hadoop.hbase.rest.client.Cluster;
import org.apache.hadoop.hbase.rest.client.RemoteAdmin;
import org.apache.hadoop.hbase.rest.client.RemoteHTable;
import org.apache.hadoop.hbase.util.Bytes;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class RESTIT {
	
	/**
	 * Tests HBase connection.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hbase"}, testName="HBase REST Connection Test")
	public void hbaseRESTConnectionTest(String uri) throws Exception{
		Configuration config = HBaseConfiguration.create();
		URI u = new URI(uri);
		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
		RemoteAdmin admin = new RemoteAdmin(client, config);
		Assert.assertNotNull(admin);
		HTableDescriptor desc = new HTableDescriptor("rest_table_creation_test");
		Assert.assertFalse(admin.isTableAvailable(desc.getName()));
	}
	
	/**
	 * Tests HBase REST table creation.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hbase"}, testName="HBase REST Table Creation Test", dependsOnMethods={"hbaseRESTConnectionTest"})
	public void hbaseRESTTableCreationTest(String uri) throws Exception{
		Configuration config = HBaseConfiguration.create();
		URI u = new URI(uri);
		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
		RemoteAdmin admin = new RemoteAdmin(client, config);
		HTableDescriptor desc = new HTableDescriptor("rest_table_creation_test");
		HColumnDescriptor col = new HColumnDescriptor("default_family");
		desc.addFamily(col);
		Assert.assertFalse(admin.isTableAvailable(desc.getName()));
		admin.createTable(desc);
		Assert.assertTrue(admin.isTableAvailable(desc.getName()));
		RemoteHTable table = new RemoteHTable(client, config, "rest_table_creation_test", null);
		Assert.assertNotNull(table);
		desc = table.getTableDescriptor();
		Assert.assertNotNull(desc);
	}
	
//	/**
//	 * Tests HBase table creation.
//	 * @throws Exception Upon any error.
//	 */
//	@Test(groups={"hbase"}, testName="HBase REST Table Creation Test")
//	public void hbaseRESTTableCreationTest() throws Exception{
//		Configuration config = HBaseConfiguration.create();
//		HBaseAdmin admin = new HBaseAdmin(config);
//		Assert.assertTrue(admin.isMasterRunning());
//		HTableDescriptor desc = new HTableDescriptor("rest_table_creation_test");
//		Assert.assertFalse(admin.tableExists(desc.getName()));
//		HColumnDescriptor col = new HColumnDescriptor("default_family");
//		desc.addFamily(col);
//		admin.createTable(desc);
//		Assert.assertTrue(admin.tableExists(desc.getName()));
//		Assert.assertTrue(admin.isTableAvailable(desc.getName()));
//		Assert.assertTrue(admin.isTableEnabled(desc.getName()));
//		HTable table = new HTable(config, desc.getName());
//		Assert.assertNotNull(table);
//	}

//	/**
//	 * Tests HBase REST connection.
//	 * @param uri The server REST uri.
//	 * @throws Exception Upon any error.
//	 */
//	@Parameters({"uri"})
//	@Test(groups={"hbase"}, testName="HBase REST Connection Test", dependsOnMethods={"hbaseRESTTableCreationTest"})
//	public void hbaseRESTConnectionTest(String uri) throws Exception{
//		Configuration config = HBaseConfiguration.create();
//		URI u = new URI(uri);
//		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
//		RemoteHTable table = new RemoteHTable(client, config, "rest_table_creation_test", null);
//		Assert.assertNotNull(table);
//		HTableDescriptor desc = table.getTableDescriptor();
//		Assert.assertNotNull(desc);
//	}
	
	/**
	 * Tests HBase REST put.
	 * @param uri The server REST uri.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hbase"}, testName="HBase REST Put Test", dependsOnMethods={"hbaseRESTTableCreationTest"})
	public void hbaseRESTPutTest(String uri) throws Exception{
		Configuration config = HBaseConfiguration.create();
		URI u = new URI(uri);
		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
		RemoteHTable table = new RemoteHTable(client, config, "rest_table_creation_test", null);
		
		Put put = new Put(Bytes.toBytes("put_row"));
		put.add(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"), Bytes.toBytes("value"));
		table.put(put);
	}
	
	/**
	 * Tests HBase REST get.
	 * @param uri The server REST uri.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hbase"}, testName="HBase REST Get Test", dependsOnMethods={"hbaseRESTPutTest"})
	public void hbaseRESTGetTest(String uri) throws Exception{
		Configuration config = HBaseConfiguration.create();
		URI u = new URI(uri);
		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
		RemoteHTable table = new RemoteHTable(client, config, "rest_table_creation_test", null);
		
		Get get = new Get(Bytes.toBytes("put_row"));
		Result res = table.get(get);
		Assert.assertFalse(res.isEmpty());
		Assert.assertNotNull(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier")));
		Assert.assertEquals(Bytes.toString(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"))), "value");
	}
	
	/**
	 * Tests HBase scan.
	 * @param uri The server REST uri.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hbase"}, testName="HBase REST Scan Test", dependsOnMethods={"hbaseRESTGetTest"})
	public void hbaseRESTScanTest(String uri) throws Exception{
		Configuration config = HBaseConfiguration.create();
		URI u = new URI(uri);
		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
		RemoteHTable table = new RemoteHTable(client, config, "rest_table_creation_test", null);
		
		Put put = new Put(Bytes.toBytes("scan_row"));
		put.add(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"), Bytes.toBytes("scan"));
		table.put(put);
		
		Result res;
		Scan scan = new Scan();
		scan.addColumn(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"));
		ResultScanner rs = table.getScanner(scan);
		Iterator<Result> i = rs.iterator();
		Assert.assertTrue(i.hasNext());
		res = i.next();
		Assert.assertFalse(res.isEmpty());
		Assert.assertNotNull(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier")));
		Assert.assertEquals(Bytes.toString(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"))), "value");
		Assert.assertTrue(i.hasNext());
		res = i.next();
		Assert.assertFalse(res.isEmpty());
		Assert.assertNotNull(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier")));
		Assert.assertEquals(Bytes.toString(res.getValue(Bytes.toBytes("default_family"), Bytes.toBytes("qualifier"))), "scan");
		Assert.assertFalse(i.hasNext());
	}
	
	/**
	 * Tests HBase table deletion.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hbase"}, testName="HBase Table Deletion Test", dependsOnMethods={"hbaseRESTScanTest"})
	public void hbaseRESTTableDeletionTest(String uri) throws Exception{
		Configuration config = HBaseConfiguration.create();
		URI u = new URI(uri);
		Client client = new Client(new Cluster().add(u.getHost(), u.getPort()));
		RemoteAdmin admin = new RemoteAdmin(client, config);
		HTableDescriptor desc = new HTableDescriptor("rest_table_creation_test");
		Assert.assertTrue(admin.isTableAvailable(desc.getName()));
		//admin.disableTable(desc.getName());
		admin.deleteTable(desc.getName());
		Assert.assertFalse(admin.isTableAvailable(desc.getName()));
	}
	
	public static void main(String[] args) throws Exception{
		RESTIT it = new RESTIT();
		it.hbaseRESTConnectionTest(args[0]);
		it.hbaseRESTTableCreationTest(args[0]);
		it.hbaseRESTPutTest(args[0]);
		it.hbaseRESTGetTest(args[0]);
		it.hbaseRESTScanTest(args[0]);
		it.hbaseRESTTableDeletionTest(args[0]);
	}
}
