<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://hypobytes.com/jsp/test/hello" prefix="hello" %>
<c:choose>
	<c:when test="${not empty param.name}">
		<c:set var="name" value="${param.name}"/>
	</c:when>
	<c:otherwise>
		<c:set var="name" value="Anonymous"/>
	</c:otherwise>
</c:choose>
<html>
	<head>
		<title>Taglib Test</title>
	</head>
	<body>
		<h1><hello:hello name="${name}"/></h1>
		<p>You have been greeted by a Java Server Page using a custom Tab Library.</p>
	</body>
</html>