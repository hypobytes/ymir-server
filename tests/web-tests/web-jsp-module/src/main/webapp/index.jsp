<%@ page contentType="text/html; charset=UTF-8" %>
<%
String name = request.getParameter("name");
if(name == null || name.trim().length() == 0){
	name = "Anonymous";
}
%>
<html>
	<head>
		<title>JSP Test</title>
	</head>
	<body>
		<h1>Hello <%=name%>!</h1>
		<p>You have been greeted by a Java Server Page.</p>
	</body>
</html>