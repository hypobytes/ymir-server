/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.plugins.web.test;

import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Tests Ymir Taglib functionality.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class TaglibIT {

	/**
	 * Tests text/html Taglib page.
	 * @param url The server URL.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"url"})
	@Test(groups={"jsp"}, testName="Taglib Test")
	public void taglibTest(String url) throws Exception{
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url + "/tlib.jsp");
		HttpResponse resp = client.execute(get);
		Assert.assertNotNull(resp);
		Assert.assertNotNull(resp.getStatusLine());
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_OK);
		HttpEntity http = resp.getEntity();
		Assert.assertNotNull(http);
		Assert.assertNotNull(http.getContentType());
		Assert.assertEquals(http.getContentType().getValue(), "text/html;charset=UTF-8");
		Assert.assertTrue(http.getContentLength() > 0);
		InputStream in = http.getContent();
		StringBuffer sb = new StringBuffer();
		byte[] buff = new byte[1024];
		int read = -1;
		while((read = in.read(buff)) != -1){
			sb.append(new String(buff, 0, read, "UTF-8"));
		}
		Assert.assertEquals(sb.toString(), "<html>" +
				"\n\t<head>" +
				"\n\t\t<title>Taglib Test</title>" +
				"\n\t</head>" +
				"\n\t<body>" +
				"\n\t\t<h1>Hello Anonymous!</h1>" +
				"\n\t\t<p>You have been greeted by a Java Server Page using a custom Tab Library.</p>" +
				"\n\t</body>" +
				"\n</html>");
	}
	
	/**
	 * Tests text/html Taglib page with a parameter.
	 * @param url The server URL.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"url"})
	@Test(groups={"jsp"}, testName="Taglib Parameter Test", dependsOnMethods={"taglibTest"})
	public void taglibParamTest(String url) throws Exception{
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url + "/tlib.jsp?name=TaglibIT");
		HttpResponse resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_OK);
		HttpEntity http = resp.getEntity();
		InputStream in = http.getContent();
		StringBuffer sb = new StringBuffer();
		byte[] buff = new byte[1024];
		int read = -1;
		while((read = in.read(buff)) != -1){
			sb.append(new String(buff, 0, read, "UTF-8"));
		}
		Assert.assertEquals(sb.toString(), "<html>" +
				"\n\t<head>" +
				"\n\t\t<title>Taglib Test</title>" +
				"\n\t</head>" +
				"\n\t<body>" +
				"\n\t\t<h1>Hello TaglibIT!</h1>" +
				"\n\t\t<p>You have been greeted by a Java Server Page using a custom Tab Library.</p>" +
				"\n\t</body>" +
				"\n</html>");
	}
}