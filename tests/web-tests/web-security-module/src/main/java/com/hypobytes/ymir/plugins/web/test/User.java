/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.plugins.web.test;

import java.io.Serializable;
import java.security.Principal;


/**
 * Simple user principal.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class User implements Principal, Serializable{
	
	private static final long serialVersionUID = 1L;

	private final String name;
	
	private Group[] groups;
	
	/**
	 * Creates a new user in groups.
	 * @param name The user name, required.
	 * @param groups The groups, optional.
	 */
	public User(String name, String... groups){
		if(name == null || name.trim().length() == 0){
			throw new IllegalArgumentException("User name is required");
		}
		this.name = name.trim();
		if(groups != null){
			this.groups = new Group[groups.length];
			for(int i = 0; i < groups.length; i++){
				this.groups[i] = new Group(groups[i]);
			}
		}else{
			this.groups = new Group[0];
		}
	}

	/* (non-Javadoc)
	 * @see java.security.Principal#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Getter for the groups.
	 * @return The groups.
	 */
	public Group[] getGroups() {
		return groups;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Principal){
			Principal p = (Principal)obj;
			return name.equals(p.getName());
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(name);
		if(groups.length > 0){
			sb.append(" - ");
			for(Group group: groups){
				sb.append(group.toString() + ", ");
			}
		}
		return sb.substring(0, sb.length() - 2);
	}
}