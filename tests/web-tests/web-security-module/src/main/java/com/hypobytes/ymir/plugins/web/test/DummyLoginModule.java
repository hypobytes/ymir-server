/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.plugins.web.test;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

/**
 * Dummy login module that maps users to groups and 
 * authenticates with username as password.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class DummyLoginModule implements LoginModule {
	
	private boolean success;
	
	private Subject subject;
	
	private CallbackHandler handler;
	
	private String uname;
	
	private String passwd;
	
	private final Map<String, User> users = new HashMap<String, User>();
	
	private final Set<Principal> principals = new HashSet<Principal>();

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#initialize(javax.security.auth.Subject, javax.security.auth.callback.CallbackHandler, java.util.Map, java.util.Map)
	 */
	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler,
			Map<String, ?> sharedState, Map<String, ?> options) {
		this.subject = subject;
		this.handler = callbackHandler;
		
		if(options == null || options.isEmpty()){
			throw new IllegalArgumentException("Specify at least one user");
		}
		for(Entry<String, ?> user: options.entrySet()){
			if(user.getKey().startsWith("USER_")){
				users.put(user.getKey().substring(5), new User(user.getKey().substring(5), ((String)user.getValue()).split(",")));
			}
		}
	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#login()
	 */
	@Override
	public boolean login() throws LoginException {
		success = false;
		Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("uname");
		callbacks[1] = new PasswordCallback("passwd", false);
		
		try {
			handler.handle(callbacks);
		} catch (IOException e) {
			throw (LoginException) new LoginException().initCause(e);
		} catch (UnsupportedCallbackException e) {
			throw (LoginException) new LoginException().initCause(e);
		}
		
		uname = ((NameCallback) callbacks[0]).getName();
		char[] pwd = ((PasswordCallback) callbacks[1]).getPassword();
		passwd = (pwd != null ? new String(pwd) : null);
		
		if((uname == null || uname.trim().length() == 0) || 
				(passwd == null || passwd.trim().length() == 0)){
			uname = null;
			passwd = null;
			throw new FailedLoginException();
		}
		uname = uname.trim();
		passwd = passwd.trim();
		
		if(!users.containsKey(uname) || !uname.equals(passwd)){
			uname = null;
			passwd = null;
			throw new FailedLoginException();			
		}
		success = true;
		return success;
	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#commit()
	 */
	@Override
	public boolean commit() throws LoginException {
		if(success){
			principals.add(new User(uname));
			User user = users.get(uname);
			for(Group group: user.getGroups()){
				principals.add(new Group(group.getName()));
			}
			subject.getPrincipals().addAll(principals);
		}
		
		uname = null;
		passwd = null;
		
		return success;
	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#abort()
	 */
	@Override
	public boolean abort() throws LoginException {
		if(success){
			uname = null;
			passwd = null;
			principals.clear();
		}
		return success;
	}

	/* (non-Javadoc)
	 * @see javax.security.auth.spi.LoginModule#logout()
	 */
	@Override
	public boolean logout() throws LoginException {
		success = false;
		uname = null;
		passwd = null;
		if(!subject.isReadOnly()){
			subject.getPrincipals().removeAll(principals);
		}
		principals.clear();
		return true;
	}
}
