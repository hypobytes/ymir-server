/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.plugins.web.test;

import java.net.URI;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Tests Ymir security functionality.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class SecurityIT {

	/**
	 * Tests access denied request.
	 * @param url The server URL.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"url"})
	@Test(groups={"security"}, testName="Security access denied Test")
	public void accessDeniedTest(String url) throws Exception{
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url + "/secure/user");
		HttpResponse resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_UNAUTHORIZED);
		HttpEntity http = resp.getEntity();
		http.consumeContent();
	}
	
	/**
	 * Tests authenticated request.
	 * @param url The server URL.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"url", "user"})
	@Test(groups={"security"}, testName="Security authenticated Test", dependsOnMethods={"accessDeniedTest"})
	public void authenticatedTest(String url, String user) throws Exception{
		UsernamePasswordCredentials auth = new UsernamePasswordCredentials(user, user);
		HttpGet get = new HttpGet(url + "/secure/user");
		URI uri = get.getURI();
		DefaultHttpClient client = new DefaultHttpClient();
		client.getCredentialsProvider().setCredentials(new AuthScope(uri.getHost(), uri.getPort()), auth);
		HttpResponse resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_OK);
		HttpEntity http = resp.getEntity();
		http.consumeContent();
	}
	
	/**
	 * Tests bad credentials request.
	 * @param url The server URL.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"url", "user"})
	@Test(groups={"security"}, testName="Security bad credentials Test", dependsOnMethods={"authenticatedTest"})
	public void badCredentialsTest(String url, String user) throws Exception{
		UsernamePasswordCredentials auth = new UsernamePasswordCredentials(user, 
				user + "_" + new Random(System.currentTimeMillis()).nextInt(100));
		HttpGet get = new HttpGet(url + "/secure/user");
		URI uri = get.getURI();
		DefaultHttpClient client = new DefaultHttpClient();
		client.getCredentialsProvider().setCredentials(new AuthScope(uri.getHost(), uri.getPort()), auth);
		HttpResponse resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_UNAUTHORIZED);
		HttpEntity http = resp.getEntity();
		http.consumeContent();
	}
	
	/**
	 * Tests forbidden request.
	 * @param url The server URL.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"url", "user", "admin"})
	@Test(groups={"security"}, testName="Security forbidden Test", dependsOnMethods={"badCredentialsTest"})
	public void forbiddenTest(String url, String user, String admin) throws Exception{
		UsernamePasswordCredentials auth = new UsernamePasswordCredentials(user, user);
		HttpGet get = new HttpGet(url + "/secure/user");
		URI uri = get.getURI();
		DefaultHttpClient client = new DefaultHttpClient();
		client.getCredentialsProvider().setCredentials(new AuthScope(uri.getHost(), uri.getPort()), auth);
		HttpResponse resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_OK);
		HttpEntity http = resp.getEntity();
		http.consumeContent();
		
		get = new HttpGet(url + "/secure/admin");
		resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_FORBIDDEN);
		http = resp.getEntity();
		http.consumeContent();
		
		auth = new UsernamePasswordCredentials(admin, admin);
		get = new HttpGet(url + "/secure/admin");
		client.getCredentialsProvider().setCredentials(new AuthScope(uri.getHost(), uri.getPort()), auth);
		resp = client.execute(get);
		Assert.assertEquals(resp.getStatusLine().getStatusCode(), HttpServletResponse.SC_OK);
		http = resp.getEntity();
		http.consumeContent();
	}
}