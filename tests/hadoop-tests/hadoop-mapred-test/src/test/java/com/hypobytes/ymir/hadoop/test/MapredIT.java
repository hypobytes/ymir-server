/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hadoop.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Tests basic MapReduce functionality.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class MapredIT {
	
//	/**
//	 * Tests Map Reduce job.
//	 * @param uri The HDFS URI.
//	 * @param input The input path.
//	 * @param output The output path.
//	 * @param jar The JAR file to put on the classpath.
//	 * @throws Exception Upon any error.
//	 */
//	@Parameters({"uri", "input", "output", "jar"})
//	@Test(groups={"mapred"}, testName="Mapred Word Count Test")
//	public void mapredWordCountTest(String uri, String input, String output, String jar) 
//			throws Exception{
//		Configuration config = new HdfsConfiguration();
//		DistributedFileSystem dfs = new DistributedFileSystem();
//		dfs.initialize(new URI(uri), config);
//		// create some input
//		Assert.assertTrue(dfs.mkdirs(new Path(input)));
//		FSDataOutputStream out;
//		out = dfs.create(new Path(input + Path.SEPARATOR + "file_01"));
//		out.write("Hello World Ugle Bjarne".getBytes());
//		out.flush(); out.close();
//		out = dfs.create(new Path(input + Path.SEPARATOR + "file_02"));
//		out.write("Hello World Nils Roger".getBytes());
//		out.flush(); out.close();
//		
//		// invoke the mapred class
//		ClassLoader loader = Thread.currentThread().getContextClassLoader();
//		Class<?> mainClass = Class.forName(WordCount.class.getName(), true, loader);
//		Method main = mainClass.getMethod("main", new Class[] {
//				Array.newInstance(String.class, 0).getClass()
//		});
//		int res = 1;
//		try {
//			Object obj = main.invoke(null, new Object[] { new String[]{input, output, jar} });
//			if(obj instanceof Integer){
//				Integer i = (Integer)obj;
//				res = i.intValue();
//			}
//		} catch (InvocationTargetException e) {
//			throw e;
//		}finally{
//			// Never reached!
//			Assert.assertEquals(res, 0);
//		}
//	}
	
	/**
	 * Verifies the output of the Mapred Word Count Test.
	 * @param uri The HDFS URI.
	 * @param output The output path.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri", "output"})
	@Test(groups={"mapred"}, testName="Mapred Word Count Verification Test")
	//, dependsOnMethods={"mapredWordCountTest"}
	public void mapredWordCountVerifyTest(String uri, String output) throws Exception{
		Configuration config = new HdfsConfiguration();
		DistributedFileSystem dfs = new DistributedFileSystem();
		dfs.initialize(new URI(uri), config);
		RemoteIterator<LocatedFileStatus> files = dfs.listFiles(new Path(output), true);
		Assert.assertTrue(files.hasNext());
		while(files.hasNext()){
			LocatedFileStatus file = files.next();
			Assert.assertNotNull(file);
		}
		Assert.assertTrue(dfs.exists(new Path(output + Path.SEPARATOR + "_SUCCESS")));
		FSDataInputStream in = dfs.open(new Path(output + Path.SEPARATOR + "part-r-00000"));
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		Assert.assertEquals(br.readLine(), "Bjarne\t1");
		Assert.assertEquals(br.readLine(), "Hello\t2");
		Assert.assertEquals(br.readLine(), "Nils\t1");
		Assert.assertEquals(br.readLine(), "Roger\t1");
		Assert.assertEquals(br.readLine(), "Ugle\t1");
		Assert.assertEquals(br.readLine(), "World\t2");
		Assert.assertNull(br.readLine());
		br.close();
	}
}
