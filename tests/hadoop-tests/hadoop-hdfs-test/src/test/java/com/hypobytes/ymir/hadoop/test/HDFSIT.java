/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package com.hypobytes.ymir.hadoop.test;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FsStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Tests basic HDFS functionality.
 * @author <a href="mailto:trygve@hypobytes.com">Trygve Sanne Hardersen</a>
 *
 */
public class HDFSIT {
	
	/**
	 * Tests HDFS connection.
	 * @param uri The server URI.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hdfs"}, testName="HDFS Connection Test")
	public void hdfsConnectionTest(String uri) throws Exception{
		Configuration config = new HdfsConfiguration();
		Assert.assertNotNull(config);
		DistributedFileSystem dfs = new DistributedFileSystem();
		Assert.assertNotNull(dfs);
		dfs.initialize(new URI(uri), config);
		FsStatus status = dfs.getStatus();
		Assert.assertNotNull(status);
	}
	
	/**
	 * Tests HDFS mkdirs command.
	 * @param uri The server URI.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hdfs"}, testName="HDFS Mkdirs Test", 
			dependsOnMethods="hdfsConnectionTest")
	public void hdfsMkdirsTest(String uri) throws Exception{
		Configuration config = new HdfsConfiguration();
		DistributedFileSystem dfs = new DistributedFileSystem();
		dfs.initialize(new URI(uri), config);
		Assert.assertTrue(dfs.mkdirs(new Path("/hdfs/mkdirs/test")));
	}
	
	/**
	 * Tests HDFS create command.
	 * @param uri The server URI.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hdfs"}, testName="HDFS Create Test", 
			dependsOnMethods="hdfsMkdirsTest")
	public void hdfsCreateTest(String uri) throws Exception{
		Configuration config = new HdfsConfiguration();
		DistributedFileSystem dfs = new DistributedFileSystem();
		dfs.initialize(new URI(uri), config);
		Assert.assertTrue(dfs.mkdirs(new Path("/hdfs/create/test")));
		FSDataOutputStream out = dfs.create(new Path("/hdfs/create/test/file"));
		out.write("This is a test".getBytes("UTF-8"));
		out.flush();
		out.close();
	}
	
	/**
	 * Tests HDFS open command.
	 * @param uri The server URI.
	 * @throws Exception Upon any error.
	 */
	@Parameters({"uri"})
	@Test(groups={"hdfs"}, testName="HDFS Open Test", 
			dependsOnMethods="hdfsCreateTest")
	public void hdfsOpenTest(String uri) throws Exception{
		Configuration config = new HdfsConfiguration();
		DistributedFileSystem dfs = new DistributedFileSystem();
		dfs.initialize(new URI(uri), config);
		FSDataInputStream in = dfs.open(new Path("/hdfs/create/test/file"));
		Assert.assertNotNull(in);
		StringBuffer sb = new StringBuffer();
		byte[] buffer = new byte[1024];
		int read = -1;
		while((read = in.read(buffer)) != -1){
			sb.append(new String(buffer, 0, read, "UTF-8"));
		}
		Assert.assertEquals(sb.toString(), "This is a test");
	}
}
